package progettosweng.shared;
import java.io.Serializable;

/**
 * Classe che rapprenseta un Funzionario Comunale
 * @author 
 *
 */
public class FunzionarioComunale extends UtenteRegistrato implements Serializable {

	private static final long serialVersionUID = -8510669295494227488L;
	
	/**
	 * Costruttore della classe Funzionario Comunale
	 * @param nome
	 * @param cognome
	 * @param telefono
	 * @param email
	 * @param codiceFiscale
	 * @param indirizzo
	 * @param dataNascita
	 * @param username
	 * @param password
	 * @param tipoDocumento
	 * @param numeroDocumento
	 * @param rilasciatoDa
	 * @param dataRilascio
	 * @param dataScadenza
	 */
	public FunzionarioComunale (String nome, String cognome, String telefono, String email, String codiceFiscale,
			   String indirizzo, String dataNascita, String username, String password,Documento documento )  {
			  super(nome, cognome, telefono, email, codiceFiscale, indirizzo, dataNascita,username,password,documento);


	}




}
