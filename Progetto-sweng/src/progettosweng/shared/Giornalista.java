package progettosweng.shared;
import java.io.Serializable;

/**
 * Classe che rapprenseta un Giornalista
 * @author 
 *
 */
public class Giornalista extends UtenteRegistrato implements Serializable  {

	private static final long serialVersionUID = -6731774678876555285L;
	
	/**
	 * Costruttore della classe Giornalista
	 * @param nome
	 * @param cognome
	 * @param telefono
	 * @param email
	 * @param codiceFiscale
	 * @param indirizzo
	 * @param dataNascita
	 * @param username
	 * @param password
	 * @param tipoDocumento
	 * @param numeroDocumento
	 * @param rilasciatoDa
	 * @param dataRilascio
	 * @param dataScadenza
	 */
	public Giornalista (String nome, String cognome, String telefono, String email, String codiceFiscale,
			   String indirizzo, String dataNascita, String username, String password,Documento documento ) {
			  super(nome, cognome, telefono, email, codiceFiscale, indirizzo, dataNascita,username,password,documento);

	}

}
