package progettosweng.shared;

import java.io.Serializable;
import java.util.*;

/**
 * Classe che rapprenseta un Voto
 * @author 
 *
 */
public class Voto implements Serializable {
	private static final long serialVersionUID = 1072897960492964258L;


	private UtenteRegistrato utente;
	private ListaElettorale lista;
	private String preferenza ;
	private Elezione elezione;

	/**
	 * Costruttore della classe Voto
	 * 
	 * @param utente username dell'utente
	 * @param lista Una Lista elettorale
	 * @param preferenza La preferenza
	 * @param elezione L'elezione
	 */
	public Voto(UtenteRegistrato utente, ListaElettorale lista,String preferenza, Elezione elezione){

		this.utente = utente;
		this.lista = lista;
		this.elezione =elezione;
		this.preferenza=preferenza;

	}


	@Override
	public String toString() {
		return "Voto [utente=" + utente + ", lista=" + lista + ", preferenza=" + preferenza + ", elezione="
				+ elezione + "]";
	}


	public UtenteRegistrato getUtente() {
		return utente;
	}


	public void setUtente(UtenteRegistrato utente) {
		this.utente = utente;
	}


	public ListaElettorale getLista() {
		return lista;
	}


	public void setLista(ListaElettorale lista) {
		this.lista = lista;
	}


	public String getPreferenza() {
		return preferenza;
	}


	public void setPreferenza(String preferenza) {
		this.preferenza = preferenza;
	}


	public Elezione getElezione() {
		return elezione;
	}


	public void setElezione(Elezione elezione) {
		this.elezione = elezione;
	}




}