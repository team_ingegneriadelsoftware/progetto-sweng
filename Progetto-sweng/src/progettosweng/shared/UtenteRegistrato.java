package progettosweng.shared;

import java.io.Serializable;

/**
 * Classe che rapprenseta un Utente Registrato
 * @author 
 *
 */
public class UtenteRegistrato extends Utente implements Serializable {

	protected String username;
	protected String password;
	private static final long serialVersionUID = 1072897960492964258L;

	/**
	 * Costruttore della classe UtenteRegistrato
	 * 
	 * @param nome
	 * @param cognome
	 * @param telefono
	 * @param email
	 * @param codiceFiscale
	 * @param indirizzo
	 * @param dataNascita
	 * @param username
	 * @param password
	 * @param tipoDocumento
	 * @param numeroDocumento
	 * @param rilasciatoDa
	 * @param dataRilascio
	 * @param dataScadenza
	 */
	public UtenteRegistrato(String nome, String cognome, String telefono, String email, String codiceFiscale,
			String indirizzo, String dataNascita, String username, String password,Documento documento) {
		super(nome, cognome, telefono, email, codiceFiscale, indirizzo, dataNascita, documento);

		this.username = username;
		this.password = password;
	}



	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	@Override
	public String toString() {
		return "UtenteRegistrato [username=" + username + ", password=" + password + ", nome=" + nome + ", cognome="
				+ cognome + ", telefono=" + telefono + ", email=" + email + ", codiceFiscale=" + codiceFiscale
				+ ", indirizzo=" + indirizzo + ", dataNascita=" + dataNascita + ", documento=" + documento + "]";
	}




}
