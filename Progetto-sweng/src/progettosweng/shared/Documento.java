package progettosweng.shared;

import java.io.Serializable;

public class Documento implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8331946555006088751L;

	private String numeroDocumento;
	private String rilascioDa;
	private String DataRilascio;
	private String DataScadenza ;
	private String tipoDocumento;

	public Documento(String tipoDocumento,String numeroDocumento, String rilascioDa, String dataRilascio, String dataScadenza) {
		super();
		this.numeroDocumento = numeroDocumento;
		this.rilascioDa = rilascioDa;
		this.DataRilascio = dataRilascio;
		this.DataScadenza = dataScadenza;
		this.tipoDocumento = tipoDocumento;
	}

	public String getNumeroDocumento() {
		return numeroDocumento;
	}

	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	public String getRilascioDa() {
		return rilascioDa;
	}

	public void setRilascioDa(String rilascioDa) {
		this.rilascioDa = rilascioDa;
	}

	public String getDataRilascio() {
		return DataRilascio;
	}

	public void setDataRilascio(String dataRilascio) {
		DataRilascio = dataRilascio;
	}

	public String getDataScadenza() {
		return DataScadenza;
	}

	public void setDataScadenza(String dataScadenza) {
		DataScadenza = dataScadenza;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	@Override
	public String toString() {
		return "Documento [numeroDocumento=" + numeroDocumento + ", rilascioDa=" + rilascioDa + ", DataRilascio="
				+ DataRilascio + ", DataScadenza=" + DataScadenza + ", tipoDocumento=" + tipoDocumento + "]";
	}

}
