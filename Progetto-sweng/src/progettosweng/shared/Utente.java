package progettosweng.shared;
import java.io.Serializable;

/**
 * Classe che rapprenseta un Utente
 * @author 
 *
 */
public class Utente implements Serializable {

	private static final long serialVersionUID = 472229872031891076L;
	protected String nome;
	protected String cognome;
	protected String telefono;
	protected String email;
	protected String codiceFiscale;
	protected String indirizzo;
	protected String dataNascita;
	protected Documento documento;


	/**
	 * Costruttore della classe Utente
	 * 
	 * @param nome
	 * @param cognome
	 * @param telefono
	 * @param email
	 * @param codiceFiscale
	 * @param indirizzo
	 * @param dataNascita
	 * @param tipoDocumento
	 * @param numeroDocumento
	 * @param rilasciatoDa
	 * @param dataRilascio
	 * @param dataScadenza
	 */
	public Utente(String nome, String cognome, String telefono, String email, String codiceFiscale, String indirizzo,
			String dataNascita, Documento documento) {

		this.nome = nome;
		this.cognome = cognome;
		this.telefono = telefono;
		this.email = email;
		this.codiceFiscale = codiceFiscale;
		this.indirizzo = indirizzo;
		this.dataNascita = dataNascita;
		this.documento = documento;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}


	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getTelefono() {
		return telefono;
	}


	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getCodiceFiscale() {
		return codiceFiscale;
	}


	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}


	public String getIndirizzo() {
		return indirizzo;
	}


	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(String dataNascita) {
		this.dataNascita = dataNascita;
	}

	public Documento getDocumento() {
		return documento;
	}


	public void setDocumento(Documento documento) {
		this.documento = documento;
	}


	@Override
	public String toString() {
		return "Utente [nome=" + nome + ", cognome=" + cognome + ", telefono=" + telefono + ", email=" + email
				+ ", codiceFiscale=" + codiceFiscale + ", indirizzo=" + indirizzo + ", dataNascita=" + dataNascita
				+ "]";
	}

}
