package progettosweng.shared;

import java.util.ArrayList;
import java.io.Serializable;

/**
 * Classe che rapprenseta una Lista Elettorale
 * @author 
 *
 */
public class ListaElettorale implements Serializable {

	private static final long serialVersionUID = 1072897960492964258L;

	private String simbolo;
	private String nome;
	private UtenteRegistrato sindaco;
	private ArrayList<UtenteRegistrato> candidati;
	private Elezione elezione;
	private String stato;
	private FunzionarioComunale FunzionarioComunale;
	private UtenteRegistrato Richiedente;
	
	/**
	 * Costruttore della classe ListaElettorale
	 * @param nome
	 * @param simbolo
	 * @param stato
	 * @param sindaco
	 * @param candidati
	 * @param elezione
	 * @param FunzionarioComunale
	 * @param Richiedente
	 */
	public ListaElettorale(String nome,String simbolo,String stato,UtenteRegistrato sindaco, ArrayList<UtenteRegistrato> candidati,Elezione elezione,FunzionarioComunale FunzionarioComunale,UtenteRegistrato Richiedente) {

		this.simbolo= simbolo;
		this.nome=nome;
		this.stato=stato;
		this.sindaco = sindaco;
		this.candidati=candidati;
		this.elezione=elezione;
		this.FunzionarioComunale = FunzionarioComunale;
		this.Richiedente = Richiedente;


	}

	public String getSimbolo() {
		return simbolo;
	}

	public void setSimbolo(String simbolo) {
		this.simbolo = simbolo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public UtenteRegistrato getSindaco() {
		return sindaco;
	}

	public void setSindaco(UtenteRegistrato sindaco) {
		this.sindaco = sindaco;
	}

	public ArrayList<UtenteRegistrato> getCandidati() {
		return candidati;
	}

	public void setCandidati(ArrayList<UtenteRegistrato> candidati) {
		this.candidati = candidati;
	}

	public Elezione getElezione() {
		return elezione;
	}

	public void setElezione(Elezione elezione) {
		this.elezione = elezione;
	}

	public String getStato() {
		return stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public FunzionarioComunale getFunzionarioComunale() {
		return FunzionarioComunale;
	}

	public void setFunzionarioComunale(FunzionarioComunale funzionarioComunale) {
		FunzionarioComunale = funzionarioComunale;
	}

	public UtenteRegistrato getRichiedente() {
		return Richiedente;
	}

	public void setRichiedente(UtenteRegistrato richiedente) {
		Richiedente = richiedente;
	}

	@Override
	public String toString() {
		return "ListaElettorale [simbolo=" + simbolo + ", nome=" + nome + ", sindaco=" + sindaco + ", candidati="
				+ candidati + ", elezione=" + elezione + ", stato=" + stato + ", FunzionarioComunale=" + FunzionarioComunale
				+ ", Richiedente=" + Richiedente + "]";
	}









}