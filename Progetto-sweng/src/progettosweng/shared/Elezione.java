package progettosweng.shared;

import java.io.Serializable;

/**
 * Classe che rapprenseta un'Elezione
 * @author 
 *
 */
public class Elezione implements  Serializable {

	private static final long serialVersionUID = -6616860500461675519L;

	private String nome;
	private String dataInizio;
	private String dataFine;
	private String oraInizio;
	private String oraFine;
	private String stato;
	private FunzionarioComunale funzionario;

	/**
	 * Costruttore della classe Elezione
	 * @param nome
	 * @param dataInizio
	 * @param dataFine
	 * @param oraInizio
	 * @param oraFine
	 * @param stato
	 * @param funzionario
	 */
	public Elezione (String nome,String dataInizio,String dataFine,String oraInizio,String oraFine,String stato,FunzionarioComunale funzionario) {

		this.nome = nome;
		this.dataInizio= dataInizio;
		this.dataFine= dataFine;
		this.oraInizio=oraInizio;
		this.oraFine=oraFine;
		this.stato=stato;
		this.funzionario = funzionario;


	}

	public FunzionarioComunale getfunzionariocomunale() {
		return funzionario;
	}



	public String getnome() {
		return nome;
	}


	public String getdataInizio() {
		return dataInizio;
	}

	public String getdataFine() {
		return dataFine;
	}

	public String getoraInizio() {
		return oraInizio;
	}

	public String getoraFine() {
		return oraFine;
	}



	public void setStato(String stato) {
		this.stato = stato;
	}


	public String getStato() {
		return stato;
	}

	@Override
	public String toString() {
		return "Elezione [nome=" + nome + ", dataInizio=" + dataInizio + ", dataFine=" + dataFine + ", oraInizio="
				+ oraInizio + ", oraFine=" + oraFine + ", stato=" + stato + ", funzionario=" + funzionario + "]";
	}










}
