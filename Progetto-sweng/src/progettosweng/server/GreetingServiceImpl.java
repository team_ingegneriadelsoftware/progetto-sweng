package progettosweng.server;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Map;
import java.util.Map.Entry;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import progettosweng.client.GreetingService;
import progettosweng.shared.*;

@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {


	private GreetingService mockito = null;


	private String risposta =null;
	private Map<String,UtenteRegistrato> UtentiMap;
	private Map<String,Elezione> ElezioniMap;
	private Map<String,ListaElettorale> ListaElettoraleMap;
	private Map<String,String> RichiesteGiornalistaMap;

	private Map<String,Voto> VotiMappa;

	private Map<String,UtenteRegistrato> RichiesteUtentiMap;

	private DB db2 =null;


	public void init(){


		db2 = DBMaker.newFileDB(new File("Database_Sweng.db")).make();

		// maps:
		UtentiMap = db2.getTreeMap("mappaUtenti");
		ElezioniMap = db2.getTreeMap("mappaElezioni");
		ListaElettoraleMap = db2.getTreeMap("mappaListe");
		VotiMappa =db2.getTreeMap("mappaVoti");
		RichiesteGiornalistaMap = db2.getTreeMap("mappaRichiesteGiornalista");
		RichiesteUtentiMap = db2.getTreeMap("mappaRichiesteRegistrazione");

		//utenti di prova
		Documento doc = new Documento("Passaporto","AS1234567890","Comune","2010-02-02","2030-10-10");
		Amministratore ur = new Amministratore("admin","admin","0513929199","admin@gmail.com","ADM628VER123","Via Saragozza 200","1990-11-23","admin","admin",doc);
		FunzionarioComunale funz = new FunzionarioComunale("Paolo","Rossi","051294938","p.rossi@gmail.com","PAROSS53HF378","Via degli Orti 12","1953-08-10","funz","funz",doc);
		UtenteRegistrato urProva1 = new UtenteRegistrato("Mirina","Rebeggiani","3343403452","miri.revenge@gmail.com","MREROSOS53HF371",
				"Via Paolo Gramsci 23","1996-12-09","ur1","ur1",doc);
		UtenteRegistrato urProva2 = new UtenteRegistrato("Alex","Renzo",
				"3343307646","gabriel.jack@gmail.com","RESOSHUAF3LL71",
				"Via della Pace 147/II ","1994-08-30","ur2","ur2",doc);

		UtentiMap.put("funz", funz);
		UtentiMap.put("admin", ur);
		UtentiMap.put("ur1", urProva1);
		UtentiMap.put("ur2", urProva2);

		db2.commit();

	}


	public int calcolaAge(String dataStr) throws IllegalArgumentException{
		int age = 0;
		String[] dateTimeArray = dataStr.split(" ");
		String[] dateArray = dateTimeArray[0].split("-");
		if(dateArray.length == 3) {

			int y = Integer.parseInt(dateArray[0]);
			int m = Integer.parseInt(dateArray[1]);
			int d = Integer.parseInt(dateArray[2]);

			String dataOggi = LocalDate.now().toString();
			String [] splitDataOggi = dataOggi.split("-");

			int yOggi = Integer.parseInt(splitDataOggi[0]);
			int mOggi = Integer.parseInt(splitDataOggi[1]);
			int dOggi = Integer.parseInt(splitDataOggi[2]);

			age = yOggi - y ;

			if(m > mOggi ) {
				age--;
			}else {
				if(m == mOggi) {
					if(d > dOggi) {
						age--;

					}
				}
			}
		}
		return age;
	}



	@Override
	public String login(String username, String password)
			throws IllegalArgumentException {

		risposta = "0";

		if(UtentiMap.get(username) != null) {
			UtenteRegistrato ur = UtentiMap.get(username);

			if(ur.getPassword().equals(password)){


				if((ur.getClass() == Amministratore.class)) {
					risposta = "2";
				}

				if((ur.getClass() == FunzionarioComunale.class)) {
					risposta = "1";
				}

				if((ur.getClass() == UtenteRegistrato.class)) {
					risposta = "3";	
				}

				if((ur.getClass() == Giornalista.class)) {
					risposta = "4";
				}

			}else {
				risposta = "-1";
			}

		}

		return risposta;

	}

	@Override
	public 	ArrayList<String> getUtentiRegistrati()throws IllegalArgumentException {

		ArrayList<String> listaUtentiRegistrati =  new ArrayList<String>();
		String buffer="";

		for(Map.Entry<String, UtenteRegistrato> entry : UtentiMap.entrySet()) {

			buffer="";

			String key = entry.getKey();

			if(UtentiMap.entrySet().getClass() != Amministratore.class) {

				UtenteRegistrato value = entry.getValue();
				buffer+=key+" : ";
				buffer+=value.getNome()+" "+value.getCognome();

				listaUtentiRegistrati.add(buffer);

			}
		}

		return listaUtentiRegistrati;
	}

	@Override
	public String registrazione(ArrayList<String> lista)
			throws IllegalArgumentException {
		
		System.out.println("RICHIESTA Registrazione: "+  lista.get(0));

		risposta = "Errore";

		if(UtentiMap.get(lista.get(0)) == null ) {

			int age=calcolaAge(lista.get(8));
			if(age >= 18) {

				UtenteRegistrato ur = new UtenteRegistrato(

						lista.get(2),
						lista.get(3),
						lista.get(4),
						lista.get(5),
						lista.get(6),
						lista.get(7),
						lista.get(8),
						lista.get(0),
						lista.get(1),
						new Documento(
								lista.get(9),
								lista.get(10),
								lista.get(11),
								lista.get(12),
								lista.get(13)
								)
						);

				RichiesteUtentiMap.put(lista.get(0), ur);

				db2.commit();

				risposta ="OK";
				System.out.println("Richiesta di Registrazione: "+RichiesteUtentiMap.get(ur.getUsername()).toString());
			}else {
				risposta ="L'utente deve essere maggiorenne";
			}

		}

		return risposta;

	}



	@Override
	public String bandisciElezione(ArrayList<String> datiElezione,String username)
			throws IllegalArgumentException {

		risposta = "Errore, Elezione non inserita";

		//controllo esistenza utente
		if(ElezioniMap.get(datiElezione.get(0)) == null ) {

			FunzionarioComunale funzionario = (FunzionarioComunale) UtentiMap.get(username);
			String stato = "In corso";

			String splitDataInizio []; 
			splitDataInizio = datiElezione.get(1).split(" ");


			String splitDataFine []; 
			splitDataFine = datiElezione.get(2).split(" ");

			String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());

			SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			Date current = null;
			Date End = null;
			Date start = null;


			try {
				End  = formatter.parse(datiElezione.get(2));
				current = formatter.parse(timeStamp);
				start = formatter.parse(datiElezione.get(1));

			} catch (ParseException e1) {

				e1.printStackTrace();
			}

			if((End.after(current)) && (End.after(start))  ){

				Elezione el = new Elezione(
						datiElezione.get(0),
						splitDataInizio[0],
						splitDataFine[0],
						splitDataInizio[1],
						splitDataFine[1],
						stato,
						funzionario

						);

				ElezioniMap.put(datiElezione.get(0), el);

				db2.commit();

				risposta ="OK,Elezione inserita correttamente";

				System.out.println("Elezione creata: " +ElezioniMap.get( datiElezione.get(0)).toString());


			}else {
				risposta = "Data non valida!!!";
			}
		}

		return risposta;
	}



	@Override
	public String presentaLista(ArrayList<String> datiLista,String usernameRichiedente)
			throws IllegalArgumentException {


		risposta = "";

		ArrayList<UtenteRegistrato> listacandidati = new ArrayList<UtenteRegistrato>();

		String [] elezione = datiLista.get(0).split("\t");

		String nomeElezione = elezione[0];

		if(ListaElettoraleMap.get(nomeElezione+"-"+(datiLista.get(1))) == null ) {

			int controllo = 0;

			String [] candidati = datiLista.get(4).split("\t");

			UtenteRegistrato sindaco = null;
			UtenteRegistrato candidato = null;


			if((candidati.length)!= 1 && (elezione.length)!= 1) {

				for(int i=0; i < candidati.length;i++) {

					for(Map.Entry<String, UtenteRegistrato> entry : UtentiMap.entrySet()) {


						String key = entry.getKey().trim();
						String[] splitUsernameCandidati = (candidati[i]).split(":");
						String[] splitUsernameSindaco = (datiLista.get(3)).split(":");
						String Username = splitUsernameCandidati[0].trim();


						if(key.equalsIgnoreCase(Username)) {

							candidato = entry.getValue();


						}

						String nmsindaco = splitUsernameSindaco[0].trim();

						if(key.equalsIgnoreCase(nmsindaco)) {

							sindaco = entry.getValue();

						}

					}

					if(sindaco != null && candidato != null) {
						if(!candidato.getUsername().equalsIgnoreCase(sindaco.getUsername())) {

							listacandidati.add(candidato);

						} else {
							controllo = 1;
						}
					}
				}

				Elezione e = ElezioniMap.get(elezione[0].trim());

				UtenteRegistrato Richiedente = UtentiMap.get(usernameRichiedente);
				FunzionarioComunale FunzionarioComunale = null;
				if(controllo == 0) {
					ListaElettorale le = new ListaElettorale(datiLista.get(1),datiLista.get(2),"Pendente",sindaco,listacandidati,e,FunzionarioComunale,Richiedente);

					String key = e.getnome()+"-"+datiLista.get(1);

					System.out.println("NEW Lista Elettorale : "+le.toString());

					ListaElettoraleMap.put(key, le);

					db2.commit();

					risposta ="OK, lista elettorale inserita ";
				}else {
					risposta ="Errore sindaco = candidato";
				}
			}else {

				risposta ="Inserisci dei candidati";

			}

		}else {
			risposta ="Errore : lista gia' inserita";
		}

		return risposta;
	}

	@Override
	public String RichiestaAccreditamentoGiornalista(String username) throws IllegalArgumentException {

		risposta = "";

		if(RichiesteGiornalistaMap.get(username)!= null ) {

			risposta = "Errore richiesta gia' inviata";

		}else {
			RichiesteGiornalistaMap.put(username,username);
			risposta = "Richiesta inviata al funzionario comunale!";
			db2.commit();	
		}

		return risposta;
	}

	@Override
	public ArrayList<String> getListaRichiesteGiornalista() throws IllegalArgumentException {

		ArrayList<String> listarichiesteGiornalisti =  new ArrayList<String>();

		for(Map.Entry<String,String> entry : RichiesteGiornalistaMap.entrySet()) {

			listarichiesteGiornalisti.add(entry.getValue());

		}

		return listarichiesteGiornalisti;

	}



	@Override
	public String setApprovaRichiestaGiornalista(String username) throws IllegalArgumentException {
		risposta = "";
		int controllo = 0;
		String key = "";
		UtenteRegistrato ur = null;
		for(Map.Entry<String, UtenteRegistrato> entry : UtentiMap.entrySet()) {

			key = entry.getKey();

			if(key.equalsIgnoreCase(username)) {
				ur = entry.getValue();
				controllo = 1;
				break;

			}

		}
		if(controllo == 1) {

			risposta = "L'utente : " +key+"  e' stato accreditato come giornalista";
			RichiesteGiornalistaMap.remove(key);

			UtentiMap.remove(key);
			db2.commit();

			Giornalista giornalista = new Giornalista(ur.getNome(),ur.getCognome(),ur.getTelefono(),ur.getEmail(),ur.getCodiceFiscale(),ur.getIndirizzo(),
					ur.getDataNascita(),ur.getUsername(),ur.getPassword(),
					new Documento(ur.getDocumento().getTipoDocumento(),ur.getDocumento().getNumeroDocumento(),ur.getDocumento().getRilascioDa(),ur.getDocumento().getDataRilascio(),ur.getDocumento().getDataScadenza()));

			UtentiMap.put(key,giornalista);
			db2.commit();

		}

		return risposta;
	}
	@Override
	public String setRifiutaRichiestaGiornalista(String username) throws IllegalArgumentException {
		risposta = "";

		String key = username;
		risposta="Richiesta rigettata per utente "+username;
		RichiesteGiornalistaMap.remove(key);

		return risposta;
	}

	@Override
	public String getInfoUsername(String username) throws IllegalArgumentException {

		risposta = "";
		String key = "";
		UtenteRegistrato ur = null;
		for(Map.Entry<String, UtenteRegistrato> entry : UtentiMap.entrySet()) {

			key = entry.getKey();

			if(key.equalsIgnoreCase(username.trim())) {
				ur = entry.getValue();
				risposta = "Nome : "+ur.getNome() +"\nCognome : "+ur.getCognome();
				break;
			}
		}

		return risposta;
	}

	@Override
	public 	ArrayList<String> getListaElettorale() throws IllegalArgumentException {

		ArrayList<String> listaListaElettorale =  new ArrayList<String>();

		for(Map.Entry<String,ListaElettorale> entry : ListaElettoraleMap.entrySet()) {

			String key = entry.getKey();

			ListaElettorale value = entry.getValue();

			if(value.getFunzionarioComunale() != null ) {
				listaListaElettorale.add(value.getNome()+"\t"+value.getSimbolo()+"\t"+value.getStato()+"\t"+value.getCandidati()+"\t"+(value.getElezione()).getnome()+"\t"+value.getRichiedente().getUsername()+"\t"+value.getFunzionarioComunale().getUsername());
			}else {
				listaListaElettorale.add(value.getNome()+"\t"+value.getSimbolo()+"\t"+value.getStato()+"\t"+value.getCandidati()+"\t"+(value.getElezione()).getnome()+"\t"+value.getRichiedente().getUsername());
			}
		}

		return listaListaElettorale;
	}


	@Override
	public 	ArrayList<String> getListaElettoraleApprovata() throws IllegalArgumentException {

		ArrayList<String> listaListaElettorale =  new ArrayList<String>();

		for(Map.Entry<String,ListaElettorale> entry : ListaElettoraleMap.entrySet()) {

			String key = entry.getKey();

			ListaElettorale value = entry.getValue();

			if(value.getStato().equalsIgnoreCase("Approvata")) {

				if(value.getFunzionarioComunale() != null ) {
					listaListaElettorale.add(value.getNome()+"\t"+value.getSimbolo()+"\t"+value.getStato()+"\t"+value.getCandidati()+"\t"+(value.getElezione()).getnome()+"\t"+value.getRichiedente().getUsername()+"\t"+value.getFunzionarioComunale().getUsername());
				}else {
					listaListaElettorale.add(value.getNome()+"\t"+value.getSimbolo()+"\t"+value.getStato()+"\t"+value.getCandidati()+"\t"+(value.getElezione()).getnome()+"\t"+value.getRichiedente().getUsername());
				}
			}
		}

		return listaListaElettorale;
	}

	@Override
	public 	ArrayList<String> getElezioneInCorso() throws IllegalArgumentException {

		ArrayList<String> listaElezione =  new ArrayList<String>();


		for(Map.Entry<String,Elezione> entry : ElezioniMap.entrySet()) {

			String key = entry.getKey();

			Elezione value = entry.getValue();

			if(value.getStato().equalsIgnoreCase("In corso")) {
				listaElezione.add(value.getnome()+"\t"+value.getdataInizio()+"\t"+value.getoraInizio()+"\t"+value.getdataFine()+"\t"+value.getoraFine());
			}

		}

		return listaElezione;
	}

	@Override
	public 	ArrayList<String> getElezioniConcluse(String data) throws IllegalArgumentException {

		changeStato(data);

		ArrayList<String> listaElezione =  new ArrayList<String>();

		for(Map.Entry<String,Elezione> entry : ElezioniMap.entrySet()) {

			String key = entry.getKey();

			Elezione value = entry.getValue();

			if(value.getStato().equalsIgnoreCase("chiuso")) {
				listaElezione.add(value.getnome()+"\t"+value.getdataInizio()+"\t"+value.getoraInizio()+"\t"+value.getdataFine()+"\t"+value.getoraFine());
			}

		}

		return listaElezione;
	}

	@Override
	public String getInfoLista(String lista) throws IllegalArgumentException {

		String listaElezione = "";

		if(ListaElettoraleMap.get(lista)!= null) {
			ListaElettorale value = ListaElettoraleMap.get(lista);

			String buffer = "";
			for(int i = 0;i < value.getCandidati().size();i++) {
				buffer+=value.getCandidati().get(i).getUsername()+" ;";
			}

			listaElezione = ("Nome: "+value.getNome()+"\n Simbolo "+value.getSimbolo()+"\nSindaco :"+value.getSindaco().getUsername()+"\n Candidati:"+buffer+"\n Richiedente: "+value.getRichiedente().getUsername());

		}

		return listaElezione;
	}



	@Override
	public String setRichiestaLista(String lista,int tipo,String usernameFunzionarioComunale) throws IllegalArgumentException {
		risposta = "";

		ListaElettorale le = null;
		String key = "";
		for(Map.Entry<String,ListaElettorale> entry : ListaElettoraleMap.entrySet()) {

			key = entry.getKey();
			le = entry.getValue();

			System.out.println(key+"controllo"+lista.trim());
			if(key.equalsIgnoreCase(lista.trim())) {

				le = entry.getValue();
				break;

			}

		}

		FunzionarioComunale Funzionario = (FunzionarioComunale) UtentiMap.get(usernameFunzionarioComunale);

		if(!Funzionario.getUsername().equalsIgnoreCase(le.getRichiedente().getUsername())) {

			switch (tipo) {
			case 0 :

				le.setStato("Approvata");
				risposta = "OK,Lista approvata";
				break;
			case 1 :

				le.setStato("Rigettata");
				risposta = "DOH,Lista rigettata";
				break;


			default :risposta="Errore"; 
			break;

			}
			le.setFunzionarioComunale(Funzionario);
			ListaElettoraleMap.remove(key);
			db2.commit();
			ListaElettoraleMap.put(key, le);

			db2.commit();

		}else {

			risposta = "Non puoi approvare/rigettare le tue richieste";

		}
		return risposta;
	}



	@Override
	public ArrayList<String> getCandidatiDisponibili() throws IllegalArgumentException {

		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> listCandidatiNonDisponibili = new ArrayList<String>();
		ArrayList<String> listSindaci = getSindaci();


		for(Map.Entry<String,ListaElettorale> entry : ListaElettoraleMap.entrySet()) {

			ListaElettorale le = entry.getValue();

			for(UtenteRegistrato u : le.getCandidati()) {
				listCandidatiNonDisponibili.add(u.getUsername());

			}
		}

		for(String i : listSindaci) {

			listCandidatiNonDisponibili.add(i);

		}

		for(Entry<String, UtenteRegistrato> entry : UtentiMap.entrySet()) {

			int controllo = 0;

			UtenteRegistrato ut = entry.getValue();

			if(!(ut.getClass() == Amministratore.class)) {

				for(String i : listCandidatiNonDisponibili) {

					if(ut.getUsername().equalsIgnoreCase(i)) {
						controllo = 1;

					}
				}
				if(controllo == 0) {
					list.add(ut.getUsername());

				}
			}
		}

		return list;
	}


	@Override
	public ArrayList<String> getSindaci() throws IllegalArgumentException {
		ArrayList <String> listaSindaci = new ArrayList <String>();

		for(Map.Entry<String,ListaElettorale> entry : ListaElettoraleMap.entrySet()) {

			ListaElettorale le = entry.getValue();
			listaSindaci.add(le.getSindaco().getUsername());

		}
		return listaSindaci ;
	}

	@Override
	public ArrayList<String> getUtentiNotSindaci() throws IllegalArgumentException {
		ArrayList <String> listaUtentinotSindaci = new ArrayList <String>();
		ArrayList <String> listasindaci = getSindaci();

		UtenteRegistrato utente =null;

		for(Map.Entry<String,UtenteRegistrato> entry : UtentiMap.entrySet()) {

			int controllo = 0;

			for(String s : listasindaci ) {
				utente = entry.getValue();


				if(utente.getUsername().equalsIgnoreCase(s) && !(utente.getClass() == Amministratore.class ) ) {

					controllo = 1;

				}

			}

			if(controllo == 0 ) {
				listaUtentinotSindaci.add(utente.getUsername());

			}
		}

		return listaUtentinotSindaci ;
	}



	@Override
	public String esprimiVoto(ArrayList<String> datiVoto)
			throws IllegalArgumentException {

		risposta = "";

		UtenteRegistrato utente =   UtentiMap.get(datiVoto.get(0).trim());
		Elezione  elezione = ElezioniMap.get(datiVoto.get(1).trim());
		ListaElettorale  nmLista = ListaElettoraleMap.get(datiVoto.get(1).trim()+"-"+datiVoto.get(2).trim());

		String preferenza = datiVoto.get(3);

		if(VotiMappa.get(utente.getUsername()+"-"+elezione.getnome()) == null ) {

			Voto v = new Voto(utente,nmLista, preferenza, elezione);

			String key =utente.getUsername()+"-"+elezione.getnome();

			VotiMappa.put(key, v);

			db2.commit();

			risposta ="OK, voto inserito ";
		}else {
			risposta ="ERRORE: voto gia' inserito";
		}
		return risposta;		
	}


	@Override
	public String visualizzaElezioni(ArrayList<String> datiSommario)
			throws IllegalArgumentException {

		String risposta = "";

		String  lista = datiSommario.get(0).trim();
		String elez = datiSommario.get(1).trim();


		ArrayList<String> utenti =  new ArrayList<String>();

		String buffer="";
		String [] split;
		String[] user;
		int conta = 0;

		for(Map.Entry<String, Voto> entry : VotiMappa.entrySet()) {

			String key = entry.getKey().trim();
			System.out.println("chiavi in map"+key);

			Voto v =  entry.getValue();

			Elezione EleVoto  = v.getElezione();
			String keyEl = EleVoto.getnome();

			ListaElettorale ls = v.getLista();
			String lsMap = ls.getNome();

			if(lsMap.equalsIgnoreCase(lista)  ) {

				conta = conta +1;

			}

			risposta = "voti ricevuti:"+ conta;

		}	

		return risposta;
	}


	@Override
	public String contaVotiCand(ArrayList<String> candidatoScelto)
			throws IllegalArgumentException {

		System.out.println("ricevutoCAND:"+ candidatoScelto.get(0));
		String risposta = "";

		String  elez = candidatoScelto.get(1).trim();
		String cand = candidatoScelto.get(0);


		String buffer="";
		String [] split;

		int conta = 0;

		for(Map.Entry<String, Voto> entry : VotiMappa.entrySet()) {

			buffer="";

			String key = entry.getKey().trim();
			Voto v =  entry.getValue();

			String prefeMap = v.getPreferenza();

			Elezione EleVoto  = v.getElezione();
			String keyEl = EleVoto.getnome();

			if(keyEl.equalsIgnoreCase(elez) && (prefeMap.equalsIgnoreCase(cand) )) {

				conta = conta +1;

			}

			risposta = "il candidato:"+""+cand+" ha ricevuto "+ conta + " voti";			
		}	

		return risposta;
	}


	public 	void changeStato(String  data) throws IllegalArgumentException {


		Elezione e = null;				
		for(Map.Entry<String, Elezione> entryE : ElezioniMap.entrySet()) {

			e =  entryE.getValue();

			if(e.getStato().equalsIgnoreCase("In corso") )  {

				String dataFine = e.getdataFine();
				String oraFine = e.getoraFine();

				String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
				String [] split = timeStamp.split(" ");


				SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat frm= new SimpleDateFormat("HH:mm:ss");

				Date endDate = null;
				Date current = null;



				try {
					endDate  = formatter.parse(dataFine);
					current = formatter.parse(split[0]);


				} catch (ParseException e1) {

					e1.printStackTrace();
				}

				if(current.after(endDate) ){
					e.setStato("chiuso");
					System.out.println("elezione"+e.toString()); 


				}

			}   

		}
	}

	@Override
	public String getProfiloUtente(String username) throws IllegalArgumentException {
		risposta = "";
		for(Map.Entry<String,UtenteRegistrato> entry : UtentiMap.entrySet()) {

			String key = entry.getKey();

			if(key.equalsIgnoreCase(username)) {

				UtenteRegistrato ut = entry.getValue();


				risposta = "Username : "+ut.getUsername()+
						"\n Nome : "+ut.getNome()+
						"\n Cognome : "+ut.getCognome()+
						"\n DataNascita : "+ut.getDataNascita()+
						"\n Telefono : "+ut.getTelefono()+
						"\n Email : "+ut.getEmail()+
						"\n CodiceFiscale : "+ut.getCodiceFiscale()+
						"\n Indirizzo : "+ut.getIndirizzo()+
						"\n Tipo Documento : "+ut.getDocumento().getTipoDocumento()+
						"\n n.Documento : "+ut.getDocumento().getNumeroDocumento()+
						"\n DataRilascio : "+ut.getDocumento().getDataRilascio()+
						"\n DataScadenza"+ut.getDocumento().getDataScadenza()+
						"\n Rilasciato da : "+ut.getDocumento().getRilascioDa();

				;
				break;
			}
		}

		return risposta;
	}


	@Override
	public ArrayList<String> getRichiesteUtente(String username) throws IllegalArgumentException {

		ArrayList <String> listaRichieste = new ArrayList<String>();

		for(Map.Entry<String,ListaElettorale> entry : ListaElettoraleMap.entrySet()) {



			ListaElettorale value = entry.getValue();
			if(value.getRichiedente().getUsername().equalsIgnoreCase(username)) {
				String buffer = "";
				for(int i = 0;i < value.getCandidati().size();i++) {
					buffer+=value.getCandidati().get(i).getUsername();
				}

				if(value.getStato().equalsIgnoreCase("Pendente")) {
					listaRichieste.add("Nome : "+value.getNome()+"\t Simbolo : "+value.getSimbolo()+"\t Sindaco : "+value.getSindaco().getUsername()+"\t Stato : "+value.getStato()+"\t Candidati : "+buffer+"\t Richiedente : "+value.getRichiedente().getUsername()+"\t Funzionario comunale: ");
				}else {
					listaRichieste.add("Nome : "+value.getNome()+"\t Simbolo : "+value.getSimbolo()+"\t Sindaco : "+value.getSindaco().getUsername()+"\t Stato : "+value.getStato()+"\t Candidati : "+buffer+"\t Richiedente : "+value.getRichiedente().getUsername()+"\t Funzionario comunale : "+value.getFunzionarioComunale().getUsername());
				}

			}
		}
		if(RichiesteGiornalistaMap.get(username)!= null) {

			listaRichieste.add("Richiesta Giornalista :  inviata");

		}
		if(UtentiMap.get(username).getClass() == Giornalista.class) {

			listaRichieste.add("Richiesta Giornalista :  approvata");

		}

		return listaRichieste;
	}


	@Override
	public String nominaFunzionario(String username) throws IllegalArgumentException {

		System.out.println("[RICHIESTA]: nominaFunzionario");

		try {

			UtenteRegistrato ur = UtentiMap.get(username);

			UtentiMap.put(username, new FunzionarioComunale (ur.getNome(), ur.getCognome(), ur.getTelefono(), ur.getEmail(), ur.getCodiceFiscale(), ur.getIndirizzo(), ur.getDataNascita(), username, ur.getPassword(), new Documento(ur.getDocumento().getTipoDocumento(), ur.getDocumento().getNumeroDocumento(), ur.getDocumento().getRilascioDa(), ur.getDocumento().getDataRilascio(), ur.getDocumento().getDataScadenza()) ) );
			db2.commit();

			return "Operazione eseguita con successo!";

		}catch(Exception e) {
			return e.getMessage();

		}
	}


	@Override
	public 	ArrayList<String> getUtentiRegistrati2()throws IllegalArgumentException {

		ArrayList<String> listaUtentiRegistrati =  new ArrayList<String>();
		String buffer="";

		for(Map.Entry<String, UtenteRegistrato> entry : UtentiMap.entrySet()) {

			buffer="";

			String key = entry.getKey();


			if( (UtentiMap.get(key).getClass() != Amministratore.class) && 
					(UtentiMap.get(key).getClass() != FunzionarioComunale.class)	) {

				UtenteRegistrato value = entry.getValue();
				buffer+=key+" : ";
				buffer+=value.getNome()+" "+value.getCognome();

				listaUtentiRegistrati.add(buffer);

			}

		}


		return listaUtentiRegistrati;
	}



	@Override
	public ArrayList<String> getRichiesteRegistrazione() throws IllegalArgumentException {

		System.out.println(" [GET] Richieste Registrazione!!!");

		ArrayList<String> listaRichieste =  new ArrayList<String>();
		String buffer="";

		for(Map.Entry<String, UtenteRegistrato> entry : RichiesteUtentiMap.entrySet()) {

			buffer="";

			String key = entry.getKey();
			UtenteRegistrato value = entry.getValue();

			buffer+=key+" : ";
			buffer+=value.getNome()+" "+value.getCognome();

			listaRichieste.add(buffer);

		}

		return listaRichieste;

	}

	@Override
	public String RifiutaRichiestaRegistrazione(String username) throws IllegalArgumentException {

		System.out.println(" [DEL] Richiesta Registrazione per: "+username);

		if( RichiesteUtentiMap.get(username)!=  null ) {

			RichiesteUtentiMap.remove(username);
			db2.commit();
			return "Operazione eseguita con successo!";
		}else {
			return "[Server]: Utente non trovato!";
		}
	}


	@Override
	public String ApprovaRichiestaRegistrazione(String username) throws IllegalArgumentException {

		System.out.println("[RICHIESTA]: Approva Richiesta Registrazione per: "+username);

		try {

			UtenteRegistrato ur = RichiesteUtentiMap.get(username);
			UtentiMap.put(username, ur);
			RichiesteUtentiMap.remove(username);

			db2.commit();
			return "Operazione eseguita con successo!";

		}catch(Exception e) {
			return e.getMessage();

		}

	}


	public ArrayList <String> sortListAsc (ArrayList<String> list){

		for( int i=0;i<list.size();i++) {

			String [] itemSplit = list.get(i).split("-");

			int conta = Integer.parseInt(itemSplit[1]);

			for( int x=1;x<list.size();x++) {
				String [] itemSplit2 = list.get(x).split("-");

				int conta2 = Integer.parseInt(itemSplit2[1]);

				if(!list.get(i).equalsIgnoreCase(list.get(x))) {

					if(conta < conta2 ) {

						String temp =list.get(i);

						list.set(i,list.get(x));

						list.set(x,temp);

					}

				}

			}

		}

		return list;

	}

	@Override
	public ArrayList<String> getReport(String idElezione) throws IllegalArgumentException {


		ArrayList <String> Report = new ArrayList <String>();

		ArrayList <String> range18_24VotiLista = new ArrayList <String>();
		ArrayList <String> range25_40VotiLista = new ArrayList <String>();
		ArrayList <String> range40_56VotiLista = new ArrayList <String>();
		ArrayList <String> range56VotiLista = new ArrayList <String>();


		ArrayList <String> range18_24VotiPreferenza = new ArrayList <String>();
		ArrayList <String> range25_40VotiPreferenza = new ArrayList <String>();
		ArrayList <String> range40_56VotiPreferenza = new ArrayList <String>();
		ArrayList <String> range56VotiPreferenza = new ArrayList <String>();


		for(Entry<String, Voto> entry : VotiMappa.entrySet()) {

			Voto v = entry.getValue();
			String [] doublekey = entry.getKey().split("-");
			String username = doublekey[0];
			String elezione = doublekey[1];

			UtenteRegistrato ut = UtentiMap.get(username);
			String datastr = ut.getDataNascita();
			int age = calcolaAge(datastr);

			if(v.getElezione().getnome().equalsIgnoreCase(idElezione)) {
				if(age >= 18 && age <=24) {

					int controllo = 0;
					int count = 1;
					int index = 0;

					for(int i = 0;i<range18_24VotiLista.size();i++) {

						String [] valore = range18_24VotiLista.get(i).split("-");

						if(v.getLista().getNome().equalsIgnoreCase(valore[0])) {
							controllo = 1;
							String [] arr  = range18_24VotiLista.get(i).split("-");
							index = range18_24VotiLista.indexOf(range18_24VotiLista.get(i));
							count = (Integer.parseInt(arr[1]))+1;
						}

					}
					if(controllo == 0) {

						range18_24VotiLista.add(v.getLista().getNome()+"-"+count);


					}else {
						range18_24VotiLista.remove(index);
						range18_24VotiLista.add(v.getLista().getNome()+"-"+count);

					}

					//inserisco la preferenza
					controllo = 0;
					count = 1;
					index = 0;

					for(int i = 0;i<range18_24VotiPreferenza.size();i++) {

						String [] valore = range18_24VotiPreferenza.get(i).split("-");

						if(v.getPreferenza().equalsIgnoreCase(valore[0])) {
							controllo = 1;
							String [] arr  = range18_24VotiPreferenza.get(i).split("-");
							index = range18_24VotiPreferenza.indexOf(range18_24VotiPreferenza.get(i));
							count = (Integer.parseInt(arr[1]))+1;
						}

					}

					if(controllo == 0) {

						range18_24VotiPreferenza.add(v.getPreferenza()+"-"+count);


					}else {
						range18_24VotiPreferenza.remove(index);
						range18_24VotiPreferenza.add(v.getPreferenza()+"-"+count);

					}

				}

				if(age >= 25 && age <=40) {


					int controllo = 0;
					int count = 1;
					int index = 0;

					for(int i = 0;i<range25_40VotiLista.size();i++) {

						String [] valore = range25_40VotiLista.get(i).split("-");

						if(v.getPreferenza().equalsIgnoreCase(valore[0])) {
							controllo = 1;
							String [] arr  = range25_40VotiLista.get(i).split("-");
							index = range25_40VotiLista.indexOf(range25_40VotiLista.get(i));
							count = (Integer.parseInt(arr[1]))+1;
						}

					}
					if(controllo == 0) {

						range25_40VotiLista.add(v.getLista().getNome()+"-"+count);


					}else {
						range25_40VotiLista.remove(index);
						range25_40VotiLista.add(v.getLista().getNome()+"-"+count);

					}

					//inserisco la preferenza
					controllo = 0;
					count = 1;
					index = 0;

					for(int i = 0;i<range25_40VotiPreferenza.size();i++) {

						String [] valore = range25_40VotiPreferenza.get(i).split("-");

						if(v.getPreferenza().equalsIgnoreCase(valore[0])) {
							controllo = 1;
							String [] arr  = range25_40VotiPreferenza.get(i).split("-");
							index = range25_40VotiPreferenza.indexOf(range25_40VotiPreferenza.get(i));
							count = (Integer.parseInt(arr[1]))+1;
						}

					}

					if(controllo == 0) {

						range25_40VotiPreferenza.add(v.getPreferenza()+"-"+count);

					}else {
						range25_40VotiPreferenza.remove(index);
						range25_40VotiPreferenza.add(v.getPreferenza()+"-"+count);

					}
				}

				if(age > 40 && age <=56) {

					int controllo = 0;
					int count = 1;
					int index = 0;

					for(int i = 0;i<range40_56VotiLista.size();i++) {

						String [] valore = range40_56VotiLista.get(i).split("-");

						if(v.getPreferenza().equalsIgnoreCase(valore[0])) {
							controllo = 1;
							String [] arr  = range40_56VotiLista.get(i).split("-");
							index = range40_56VotiLista.indexOf(range40_56VotiLista.get(i));
							count = (Integer.parseInt(arr[1]))+1;
						}

					}
					if(controllo == 0) {

						range40_56VotiLista.add(v.getLista().getNome()+"-"+count);


					}else {
						range40_56VotiLista.remove(index);
						range40_56VotiLista.add(v.getLista().getNome()+"-"+count);

					}

					//inserisco la preferenza
					controllo = 0;
					count = 1;
					index = 0;

					for(int i = 0;i<range40_56VotiPreferenza.size();i++) {

						String [] valore = range40_56VotiPreferenza.get(i).split("-");

						if(v.getPreferenza().equalsIgnoreCase(valore[0])) {
							controllo = 1;
							String [] arr  = range40_56VotiPreferenza.get(i).split("-");
							index = range40_56VotiPreferenza.indexOf(range40_56VotiPreferenza.get(i));
							count = (Integer.parseInt(arr[1]))+1;
						}

					}
					if(controllo == 0) {

						range40_56VotiPreferenza.add(v.getPreferenza()+"-"+count);


					}else {
						range40_56VotiPreferenza.remove(index);
						range40_56VotiPreferenza.add(v.getPreferenza()+"-"+count);

					}

				}

				if(age > 56) {

					int controllo = 0;
					int count = 1;
					int index = 0;

					for(int i = 0;i<range56VotiLista.size();i++) {

						String [] valore = range56VotiLista.get(i).split("-");

						if(v.getLista().getNome().equalsIgnoreCase(valore[0])) {
							controllo = 1;
							String [] arr  = range56VotiLista.get(i).split("-");
							index = range56VotiLista.indexOf(range56VotiLista.get(i));
							count = (Integer.parseInt(arr[1]))+1;
						}

					}
					if(controllo == 0) {

						range56VotiLista.add(v.getLista().getNome()+"-"+count);


					}else {
						range56VotiLista.remove(index);
						range56VotiLista.add(v.getLista().getNome()+"-"+count);

					}

					//inserisco la preferenza
					controllo = 0;
					count = 1;
					index = 0;

					for(int i = 0;i<range56VotiPreferenza.size();i++) {

						String [] valore = range56VotiPreferenza.get(i).split("-");

						if(v.getPreferenza().equalsIgnoreCase(valore[0])) {
							controllo = 1;
							String [] arr  = range56VotiPreferenza.get(i).split("-");
							index = range56VotiPreferenza.indexOf(range56VotiPreferenza.get(i));
							count = (Integer.parseInt(arr[1]))+1;
						}

					}
					if(controllo == 0) {

						range56VotiPreferenza.add(v.getPreferenza()+"-"+count);

					}else {
						range56VotiPreferenza.remove(index);
						range56VotiPreferenza.add(v.getPreferenza()+"-"+count);

					}
				}
			}
		}
		//SORT PREFERENZE
		sortListAsc(range18_24VotiPreferenza);
		sortListAsc(range25_40VotiPreferenza);
		sortListAsc(range40_56VotiPreferenza);
		sortListAsc(range56VotiPreferenza);

		//SORT VOTI
		sortListAsc(range18_24VotiLista);
		sortListAsc(range25_40VotiLista);
		sortListAsc(range40_56VotiLista);
		sortListAsc(range56VotiLista);

		Report.add("<h3>18-24 Voti :</h3>");

		for(String x : range18_24VotiLista ) {
			String [] arr = x.split("-");
			Report.add(arr[0]+"&emsp;&emsp; voti : "+arr[1]);
		}

		Report.add("<h3>18-24 Preferenza :</h3>");

		for(String x : range18_24VotiPreferenza) {
			String [] arr = x.split("-");
			Report.add(arr[0]+"&emsp;&emsp; voti : "+arr[1]);
		}

		Report.add("<h3>25-40 Voti :</h3>");

		for(String x : range25_40VotiLista ) {
			String [] arr = x.split("-");
			Report.add(arr[0]+"&emsp;&emsp; voti : "+arr[1]);
		}

		Report.add("<h3>25-40 Preferenza :</h3>");

		for(String x : range25_40VotiPreferenza) {
			String [] arr = x.split("-");
			Report.add(arr[0]+"&emsp;&emsp; voti : "+arr[1]);
		}

		Report.add("<h3>40-56 Voti :</h3>");

		for(String x : range40_56VotiLista ) {
			String [] arr = x.split("-");
			Report.add(arr[0]+"&emsp;&emsp; voti : "+arr[1]);
		}

		Report.add("<h3>40-56 Preferenza :</h3>");

		for(String x : range40_56VotiPreferenza) {
			String [] arr = x.split("-");
			Report.add(arr[0]+"&emsp;&emsp; voti : "+arr[1]);
		}

		Report.add("<h3>56+ Voti :</h3>");

		for(String x : range56VotiLista) {
			String [] arr = x.split("-");
			Report.add(arr[0]+"&emsp;&emsp; voti : "+arr[1]);
		}

		Report.add("<h3>56+ Preferenza :</h3>");

		for(String x : range56VotiPreferenza) {
			String [] arr = x.split("-");
			Report.add(arr[0]+"&emsp;&emsp; voti : "+arr[1]);
		}

		return Report;
	}

	public Map<String, UtenteRegistrato> getUtentiMap() {
		return UtentiMap;
	}

	public void setUtentiMap(Map<String, UtenteRegistrato> utentiMap) {
		UtentiMap = utentiMap;
	}

	public Map<String, Elezione> getElezioniMap() {
		return ElezioniMap;
	}

	public void setElezioniMap(Map<String, Elezione> elezioniMap) {
		ElezioniMap = elezioniMap;
	}

	public Map<String, ListaElettorale> getListaElettoraleMap() {
		return ListaElettoraleMap;
	}

	public void setListaElettoraleMap(Map<String, ListaElettorale> listaElettoraleMap) {
		ListaElettoraleMap = listaElettoraleMap;
	}

	public Map<String, String> getRichiesteGiornalistaMap() {
		return RichiesteGiornalistaMap;
	}

	public void setRichiesteGiornalistaMap(Map<String, String> richiesteGiornalistaMap) {
		RichiesteGiornalistaMap = richiesteGiornalistaMap;
	}

	public Map<String, Voto> getVotiMappa() {
		return VotiMappa;
	}

	public void setVotiMappa(Map<String, Voto> votiMappa) {
		VotiMappa = votiMappa;
	}

	public Map<String, UtenteRegistrato> getRichiesteUtentiMap() {
		return RichiesteUtentiMap;
	}

	public void setRichiesteUtentiMap(Map<String, UtenteRegistrato> richiesteUtentiMap) {
		RichiesteUtentiMap = richiesteUtentiMap;
	}

	public GreetingService getMockito() {
		return mockito;
	}

	public void setMockito(GreetingService mockito) {
		this.mockito = mockito;
	}



	public DB getDb2() {
		return db2;
	}



	public void setDb2(DB db2) {
		this.db2 = db2;
	}







}
