package progettosweng.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {
	/**
	 * Metodo per consentire la registrazione da parte di un nuovo Utente
	 * @param listaDati Dati dell'utente che intende registrarsi
	 * @return Esito della richiesta di registrazione
	 * @throws IllegalArgumentException
	 */
	String registrazione(ArrayList<String> listaDati) throws IllegalArgumentException;
	/**
	 * Metodo per consentire il Login da parte di un utente registrato
	 * @param username username dell'utente registrato
	 * @param password password dell'utente registrato
	 * @return esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	String login(String username, String password) throws IllegalArgumentException;
	/**
	 * Metodo che ritorna tutti gli utenti registrati
	 * @return Lista di utente registrati
	 * @throws IllegalArgumentException
	 */
	ArrayList<String> getUtentiRegistrati( ) throws IllegalArgumentException;
	/**
	 * Metodo per bandire una determinata Elezione
	 * @param datiElezione I dati della Elezione
	 * @param username Username del Funzionario Comunale che bandisce l'elezione
	 * @return
	 * @throws IllegalArgumentException
	 */
	String bandisciElezione(ArrayList<String> datiElezione,String username) throws IllegalArgumentException;
	/**
	 * Metodo che permette di inviare una richiesta per essere accreditato come giornalista
	 * 
	 * @param username Username del giornalista
	 * @return esito della richiesta
	 * @throws IllegalArgumentException
	 */
	String RichiestaAccreditamentoGiornalista(String username)throws IllegalArgumentException;
	/**
	 * 
	 * Metodo che ritorna tutte le richieste dei giornalisti
	 * @return Lista degli usernames dei giornalisti
	 * @throws IllegalArgumentException
	 */
	ArrayList<String> getListaRichiesteGiornalista() throws IllegalArgumentException;
	/**
	 * 
	 * Metodo che consente di presentare una lista
	 * 
	 * @param datiLista Dati della lista corrente
	 * @param usernameRichiedente Username del richiedente
	 * @return Esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	String presentaLista(ArrayList<String> datiLista,String usernameRichiedente) throws IllegalArgumentException;
	/**
	 * Metodo che consente di approvare la richiesta di un giornalista
	 * @param username Username dell'utente da accreditare
	 * @return L'esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	String setApprovaRichiestaGiornalista(String username)throws IllegalArgumentException;
	/**
	 *  Metodo che consente di rifiutare la richiesta di un giornalista
	 * @param username Username dell'utente da rifiutare
	 * @return L'esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	String setRifiutaRichiestaGiornalista(String username)throws IllegalArgumentException;	
	/**
	 * Metodo che fornisce le informazioni di un determinato utente registrato
	 * @param username Username dell'utente registrato
	 * @return Le informazioni dell'utente richiesto
	 * @throws IllegalArgumentException
	 */
	String getInfoUsername(String username)throws IllegalArgumentException;
	/**
	 * Metodo che ritorna tutte le liste elettorali
	 * @return le liste elettorali presenti nel sistema
	 * @throws IllegalArgumentException
	 */
	ArrayList<String> getListaElettorale( ) throws IllegalArgumentException;
	/**
	 * Metodo che ritorna tutte le liste elettorali approvate
	 * @return Una collezione delle liste elettorali che sono state approvate
	 * @throws IllegalArgumentException
	 */
	ArrayList<String> getListaElettoraleApprovata( ) throws IllegalArgumentException;
	/**
	 * Metodo che ritorna i dati delle elezioni in corso
	 * @return Una lista dei dati delle elezioni in corso
	 * @throws IllegalArgumentException
	 */
	ArrayList<String> getElezioneInCorso( ) throws IllegalArgumentException;
	/**
	 * Metodo che ritorna i dati di tutte le elezioni concluse entro una determinata data.
	 * @param data La data da utilizzare per ricavare le elezioni concluse
	 * @return i dati delle elezioni concluse
	 * @throws IllegalArgumentException
	 */
	ArrayList<String> getElezioniConcluse(String data ) throws IllegalArgumentException;
	/**
	 * Metodo che restituisce l'informazione di una determinanta lista
	 * @param lista Nome della lista
	 * @return Informazione della lista richiesta
	 * @throws IllegalArgumentException
	 */
	String getInfoLista(String lista) throws IllegalArgumentException;
	String setRichiestaLista(String lista,int tipo,String username)throws IllegalArgumentException;	
	/**
	 * Metodo che ritorna tutti i candidati disponibili
	 * @return lista di candidati
	 * @throws IllegalArgumentException
	 */
	ArrayList<String> getCandidatiDisponibili()throws IllegalArgumentException;
	/**
	 * Metodo per ottenere tutti i sindaci presenti nel sistema
	 * @return lista di sindaci
	 * @throws IllegalArgumentException
	 */
	ArrayList<String> getSindaci()throws IllegalArgumentException;
	/**
	 * metodo che restituisce gli utenti che non sono sindaci
	 * @return lista di utenti non sindaci
	 * @throws IllegalArgumentException
	 */
	ArrayList<String> getUtentiNotSindaci()throws IllegalArgumentException;
	/**
	 * Metodo che restituisce l'informazione di un determinanto utente registrato
	 * @param username Username dell'utente registrato
	 * @return Le informazioni dell'utente registrato
	 * @throws IllegalArgumentException
	 */
	String getProfiloUtente(String username)throws IllegalArgumentException;
	/**
	 * Metodo che restituisce tutte le richieste fatte da un utente
	 * @param username Username dell'utente registrato
	 * @return lista contenente i dati delle richieste effettuate
	 * @throws IllegalArgumentException
	 */
	ArrayList<String> getRichiesteUtente(String username)throws IllegalArgumentException;
	/**
	 * Metodo che consente di nominare un Funzionario
	 * @param username username dell'utente registrato
	 * @return Esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	String nominaFunzionario(String username)throws IllegalArgumentException;
	/**
	 * Metodo che ritorna tutti gli utenti registrati che non sono ne' Amministratori ne' Funzionari comunali
	 * @return Una lista contenente gli username degli utenti registrati presenti nel sistema
	 * @throws IllegalArgumentException
	 */
	ArrayList<String> getUtentiRegistrati2( ) throws IllegalArgumentException;
	/**
	 * Metodo che restituisce tutte le richieste di registrazione
	 * @return Lista contenente le richieste di registrazione.
	 * @throws IllegalArgumentException
	 */
	ArrayList<String> getRichiesteRegistrazione( ) throws IllegalArgumentException;
	/**
	 * Metodo che rifiuta  una richiesta di registrazione
	 * @param username username dell'utente che ha inviato la richiesta
	 * @return L'esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	String RifiutaRichiestaRegistrazione(String username)throws IllegalArgumentException;	
	/**
	 * Metodo che approva una richiesta di registrazione
	 * @param username Username dell'utente che ha inviato la richiesta di registrazione
	 * @return L'esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	String ApprovaRichiestaRegistrazione(String username)throws IllegalArgumentException;	
	/**
	 * 
	 * @param datiVoto lista con i dati di un voto
	 * @return l'esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	
	String esprimiVoto(ArrayList<String> datiVoto) throws IllegalArgumentException;
	/**
	 * 
	 * @param datiVoto Lista con i dati del voto
	 * @return esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	String visualizzaElezioni(ArrayList<String> datiVoto)throws IllegalArgumentException;
	/**
	 * 
	 * @param candidatoScelto username del candidato scelto
	 * @return una stringa : esito
	 * @throws IllegalArgumentException
	 */
	String contaVotiCand(ArrayList<String> candidatoScelto)throws IllegalArgumentException;
	/**
	 * 
	 * @param idElezione id dell'Elezione
	 * @return lista di Stringhe
	 * @throws IllegalArgumentException
	 */
	ArrayList <String> getReport (String idElezione)throws IllegalArgumentException;
	
}
