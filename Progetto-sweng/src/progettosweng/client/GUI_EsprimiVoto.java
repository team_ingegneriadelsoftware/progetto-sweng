package progettosweng.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;


/**
 * GUI che consente di esprimere un voto
 * @author 
 *
 */
public class GUI_EsprimiVoto {


	private VerticalPanel vPanel = null;

	public GUI_EsprimiVoto( VerticalPanel vp) {

		this.vPanel = vp;

	}



	public void updateListboxElezione(ListBox listbox) {
		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getElezioneInCorso( new AsyncCallback < ArrayList<String> >() {


				public void onFailure(Throwable caught) {

					final DialogBox dialogBox = new DialogBox();
					dialogBox.setText("ERRORE!!"+ caught.getMessage());

				}
				@SuppressWarnings("unused")
				public void onSuccess( ArrayList<String> lista) {
					System.out.println(lista);


					for(String x : lista) {

						String[] split = x.split("\t");
						String nmElez = split[0];
						listbox.addItem(nmElez);
					}

				}

			});	
		}catch(Error e){};

	}
	public void updateListboxLista(ListBox listbox, String Elezione) {
		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getListaElettoraleApprovata(new AsyncCallback < ArrayList <String> >() {


				public void onFailure(Throwable caught) {

					final DialogBox dialogBox = new DialogBox();
					dialogBox.setText("ERRORE!!"+ caught.getMessage());

				}
				@SuppressWarnings("unused")
				@Override
				public void onSuccess( ArrayList<String> lista) {


					for(String x : lista) {
						String[] split = x.split("\t");
						String nmLista = split[0];
						String nmElezione = split[4];
						if(nmElezione.equalsIgnoreCase(Elezione)) {

							listbox.addItem(nmLista);
						}
					}

				}

			});	
		}catch(Error e){};

	}

	public void updateInfo( String candidato) {
		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);


			greetingService.getInfoUsername(candidato,new AsyncCallback <String>() {

				public void onFailure(Throwable caught) {

					final DialogBox dialogBox = new DialogBox();
					dialogBox.setText(caught.getMessage());
				}

				public void onSuccess(String risposta) {
					final MyDialogBox dialogBox = new MyDialogBox(""+risposta);

				}

			});

		}catch(Error e){};

	}



	public void onModuleLoad() {

		HTML titolo = new HTML("<h1> Esprimi Voto  </h1>");


		vPanel.add(titolo);


		final Label label1 = new Label("Seleziona lista ");
		vPanel.add(label1);

		ListBox lista = new ListBox();
		vPanel.add(lista);



		final Label label2 = new Label("Seleziona preferenza ");
		vPanel.add(label2);

		ListBox preferenza = new ListBox();
		vPanel.add(preferenza);
		preferenza.addItem("No preferenza");


		final Label label3 = new Label("Seleziona elezione ");
		vPanel.add(label3);

		ListBox elezione = new ListBox();
		vPanel.add(elezione);

		final Button buttonLista = new Button("Conferma lista");
		buttonLista.getElement().setAttribute("align", "center");

		final Button buttonElezione = new Button("Conferma elezione");
		buttonElezione.getElement().setAttribute("align", "center");

		final Button buttonInvio = new Button("Avanti");
		buttonInvio.getElement().setAttribute("align", "center");

		final Button buttonInfo = new Button("Info candidato");
		buttonInfo.getElement().setAttribute("align", "center");

		HTML spazio = new HTML("</br></br></br>");

		final Grid grid = new Grid(14, 14);

		grid.setWidget(0, 0,  label3);
		grid.setWidget(0, 1,  elezione);
		grid.setWidget(1, 0,  spazio);
		grid.setWidget(1, 1,  buttonElezione);
		grid.setWidget(2, 0,  spazio);
		grid.setWidget(2, 1,  spazio);
		grid.setWidget(3, 0,  label1);
		grid.setWidget(3, 1,  lista);
		grid.setWidget(4, 0,  spazio);
		grid.setWidget(4, 1, buttonLista );
		grid.setWidget(5, 0,  spazio);
		grid.setWidget(5, 1,  spazio);
		grid.setWidget(6, 0,  label2);
		grid.setWidget(6, 1,  preferenza);
		grid.setWidget(6, 2,  buttonInfo);
		grid.setWidget(7, 0,  spazio);
		grid.setWidget(7, 1,  buttonInvio);




		vPanel.add(grid);

		updateListboxElezione(elezione);




		buttonElezione.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int indiceElez = elezione.getSelectedIndex();
				String nmElez = elezione.getValue(indiceElez );
				lista.clear();
				updateListboxLista(lista,nmElez);

			}
		});

		buttonLista.addClickHandler(new ClickHandler() {


			@Override
			public void onClick(ClickEvent event) {


				int indicePreferenza = preferenza.getSelectedIndex();
				String nmPreferenza = preferenza.getValue(indicePreferenza );
				int indiceLista = lista.getSelectedIndex();
				String nmLista = lista.getValue(indiceLista );
				int indiceElez = elezione.getSelectedIndex();
				String nmElez = elezione.getValue(indiceElez );


				String lista = nmElez+"-"+nmLista;

				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);



				greetingService.getInfoLista(lista, new AsyncCallback<String>() {


					public void onFailure(Throwable caught) {
						// Show the RPC error message to the user

						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText("ERRORE!!"+ caught.getMessage());


					}
					@SuppressWarnings("unused")
					@Override
					public void onSuccess(String lista) {


						String [] risposta =lista.split("\n");

						preferenza.clear();	


						String[]  splittati = risposta[3].split("Candidati:");
						String cand  = splittati[1];
						String [] candidati = cand.split(";");
						preferenza.addItem("No preferenza");
						for(String x : candidati) {

							preferenza.addItem(x);
						}


					}


				});	

			}

		});

		buttonInfo.addClickHandler(new ClickHandler() {


			public void onClick(ClickEvent event) {

				int indicePreferenza = preferenza.getSelectedIndex();
				String nmPreferenza = preferenza.getValue(indicePreferenza );

				updateInfo(nmPreferenza);

			}

		});


		buttonInvio.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {


				int indiceElez = elezione.getSelectedIndex();
				String nmElez = elezione.getValue(indiceElez );

				int indiceLista = lista.getSelectedIndex();
				String nmLista = lista.getValue(indiceLista );

				int indicePreferenza = preferenza.getSelectedIndex();
				String nmPreferenza = preferenza.getValue(indicePreferenza );




				ArrayList<String> datiVoto = new ArrayList<String>();
				datiVoto.add(Passport.username);
				datiVoto.add(nmElez);
				datiVoto.add(nmLista);
				datiVoto.add(nmPreferenza);


				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);


				greetingService.esprimiVoto(datiVoto, new AsyncCallback<String>() {

					public void onFailure(Throwable caught) {
						// Show the RPC error message to the user

						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText("ERRORE!!"+ caught.getMessage());

					}

					@SuppressWarnings("unused")
					@Override
					public void onSuccess(String result) {

						lista.clear();	
						elezione.clear();	
						updateListboxElezione(elezione);


						final MyDialogBox dialogBox = new MyDialogBox("=> "+result);


					}

				});	

			}

		});


		vPanel.getElement().setAttribute("align", "center");
	
		RootPanel.get().add(vPanel);

	}

}

