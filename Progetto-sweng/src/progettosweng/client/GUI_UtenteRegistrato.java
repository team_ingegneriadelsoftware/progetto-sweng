package progettosweng.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class GUI_UtenteRegistrato {


	private VerticalPanel vPanel = null;

	/**
	 * Costruttore della classe GUI_UtenteRegistrato
	 * @param vp VerticalPanel
	 */
	public GUI_UtenteRegistrato( VerticalPanel vp) {

		this.vPanel = vp;

	}
	
	/**
	 * Carica GUI
	 */
	public void onModuleLoad() {

		HTML titolo = new HTML("<h1> Utente Registrato </h1>");

		vPanel.add(titolo);

		HorizontalPanel hPanel = new HorizontalPanel();


		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getProfiloUtente(Passport.username,new AsyncCallback<String>() {

				public void onFailure(Throwable caught) {

					final DialogBox dialogBox = new DialogBox();
					dialogBox.setText(caught.getMessage());
				}

				public void onSuccess(String info) {
					String [] arrayInfo = info.split("\n");

					String htmlProfilo = "<h2>Profilo</h2>"+
							arrayInfo[0]+"<br>"+
							arrayInfo[1]+"<br>"+
							arrayInfo[2]+"<br>"+
							arrayInfo[3]+"<br>"+
							arrayInfo[4]+"<br>"+
							arrayInfo[5]+"<br>"+
							arrayInfo[6]+"<br>"+
							arrayInfo[7]+"<br>";

					String htmlDocumento = "<h2>Documento</h2>"+
							arrayInfo[8]+"<br>"+
							arrayInfo[9]+"<br>"+
							arrayInfo[10]+"<br>"+
							arrayInfo[11]+"<br>"+
							arrayInfo[12];

					hPanel.add(new HTML(htmlProfilo));
					hPanel.add(new HTML("&emsp;&emsp;&emsp;&emsp;"));
					hPanel.add(new HTML(htmlDocumento));

					vPanel.getElement().setAttribute("align", "center");


					vPanel.add(hPanel);


				}

			});

		}catch(Error e){};

		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getRichiesteUtente(Passport.username,new AsyncCallback<ArrayList<String>>() {

				public void onFailure(Throwable caught) {

					final DialogBox dialogBox = new DialogBox();
					dialogBox.setText(caught.getMessage());
				}

				public void onSuccess(ArrayList <String> Richieste) {
					String htmlListe = "";
					String buffer = "";

					for(String i : Richieste) {

						String [] arrayRichieste = i.split("\t");

						buffer += "<br>"+i;


						htmlListe = buffer;

					}


					vPanel.add(new HTML("<hr>"));
					vPanel.add(new HTML("<h2>Richieste Effettuate : </h2>"));
					vPanel.add(new HTML(htmlListe));


				}

			});

		}catch(Error e){};

		Menu menuUtente = new Menu(this.vPanel, 3);
		menuUtente.onModuleLoad();

		vPanel.getElement().setAttribute("align", "center");
		RootPanel.get().add(vPanel);

	}

}
