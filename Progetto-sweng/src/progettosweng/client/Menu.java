package progettosweng.client;

import com.google.gwt.user.client.ui.MenuBar;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Menu{
	
	private VerticalPanel vPanel = null;
	private int tipo = -1;

	/**
	 * Costruttore della classe Menu
	 * @param vp VerticalPanel
	 * @param tipo tipo di menu da generare
	 */
	public Menu(VerticalPanel vp, int tipo) {
		this.vPanel = vp;
		this.tipo = tipo;
	}
	
	/**
	 * Carica il Menu
	 */
	public void onModuleLoad() {

		Comando cmd = new Comando(this.vPanel);
		MenuBar menu = new MenuBar();

		// Utente non registrato
		if(this.tipo == 0) {
			MenuBar menu1 = new MenuBar(true);
			menu1.addItem("🔑 Login", cmd.comando("login"));
			menu1.addItem("📝 Registrazione", cmd.comando("registrazione"));
			menu1.addItem("📊 Visualizza elezioni concluse", cmd.comando("Elezioni Concluse"));
			menu1.addItem("ℹ️ Info", cmd.comando("info"));
			menu.addItem("🏠 Menu", menu1);
		}
		// Funzionario Comunale: 1
		// Amministratore: 2
		if((this.tipo == 1) || (this.tipo == 2)) {
			
			MenuBar menu1 = new MenuBar(true);
			menu1.addItem("👤 Profilo", cmd.comando("Profilo"));
			menu1.addItem("⛔️ Logout", cmd.comando("login"));

			if(this.tipo == 1) {
				menu.addItem("👩🏼‍🏫 FUNZIONARIO COMUNALE", menu1);
			}else {
				menu.addItem("👨🏻‍🏫 AMMINISTRATORE", menu1);
			}
			menu.addItem("🙋🏻‍♀️ Esprimi voto", cmd.comando("Esprimi voto"));
			menu.addItem("📄 Presenta lista", cmd.comando("Presenta lista"));
			menu.addItem("📊 Visualizza elezioni concluse", cmd.comando("Elezioni Concluse"));
			menu.addItem("🔖 Bandisci elezioni", cmd.comando("Bandisci elezione"));
			menu.addItem("📬 Richieste", cmd.comando("Richieste"));

			if(this.tipo == 2 ) {
				menu.addItem("👉🏼 Nomina funzionario", cmd.comando("Nomina funzionario"));

			}
		}
		/*
		 * Utente registrato: 3
		 * Giornalista: 4
		 */
		if((this.tipo == 3) || (this.tipo == 4)) {
			MenuBar menu1 = new MenuBar(true);
			menu1.addItem("👤 Profilo", cmd.comando("Profilo"));
			menu1.addItem("⛔️ Logout", cmd.comando("login"));

			if(this.tipo == 3) {
				menu.addItem("👤 UTENTE REGISTRATO", menu1);
			}else {
				menu.addItem("👨🏻‍💼 GIORNALISTA", menu1);
			}
			menu.addItem("🙋🏻‍♀️ Esprimi voto", cmd.comando("Esprimi voto"));
			menu.addItem("📄 Presenta lista", cmd.comando("Presenta lista"));
			menu.addItem("📊 Visualizza elezione conclusa", cmd.comando("Elezioni Concluse"));

			if(this.tipo == 4) {
				menu.addItem("🗂 Visualizza report", cmd.comando("Visualizza report"));
			}else {
				menu.addItem("📤 Richiesta giornalista", cmd.comando("Richiesta giornalista"));
			}
		}
		menu.setSize("120em", "2.5em");
		RootPanel.get().add(menu, 0, 0);
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	 
}


