package progettosweng.client;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Calendar;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class GUI_ElezioniConcluse {

	private VerticalPanel vPanel = null;

	/**
	 * costruttore della classe GUI_ElezioniConcluse
	 * @param vp VerticalPanel
	 */
	public GUI_ElezioniConcluse( VerticalPanel vp) {

		this.vPanel = vp;

	}
	
	/**
	 * Metodo che aggiorna la ListBox delle elezioni
	 * @param listbox ListBox
	 */

	public void updateListboxElezione(ListBox listbox) {

		Date date = new Date();

		DateTimeFormat dtf = DateTimeFormat.getFormat("yyyy-MM-dd hh:mm:ss");
		String data = dtf.format(date, TimeZone.createTimeZone(0));


		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getElezioniConcluse(data, new AsyncCallback < ArrayList<String> >() {


				public void onFailure(Throwable caught) {

					final DialogBox dialogBox = new DialogBox();
					dialogBox.setText("ERRORE!!"+ caught.getMessage());

				}
				@SuppressWarnings("unused")
				public void onSuccess( ArrayList<String> lista) {
					System.out.println(lista);


					for(String x : lista) {

						String[] split = x.split("\t");
						String nmElez = split[0];
						listbox.addItem(nmElez);
					}

				}

			});	
		}catch(Error e){};

	}

	/**
	 * Metodo che aggiorna la listbox
	 * @param listbox
	 */
	public void updateListboxLista(ListBox listbox, String Elezione) {
		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getListaElettorale(new AsyncCallback < ArrayList <String> >() {


				public void onFailure(Throwable caught) {

					final DialogBox dialogBox = new DialogBox();
					dialogBox.setText("ERRORE!!"+ caught.getMessage());

				}
				@SuppressWarnings("unused")
				@Override
				public void onSuccess( ArrayList<String> lista) {


					for(String x : lista) {
						String[] split = x.split("\t");
						String nmLista = split[0];
						String nmElezione = split[4];
						if(nmElezione.equalsIgnoreCase(Elezione)) {

							listbox.addItem(nmLista);
						}
					}

				}

			});	
		}catch(Error e){};

	}
	
	/**
	 * carica la GUI
	 */
	public void onModuleLoad() {

		HTML titolo = new HTML("<h1>  Elezioni Concluse  </h1>");


		final Label label0 = new Label("Seleziona Elezione ");
		ListBox Elez = new ListBox();


		final Label label1 = new Label("Seleziona lista ");
		ListBox lista = new ListBox();

		final Label label2 = new Label("Seleziona Candidato ");
		ListBox listaCand = new ListBox();



		final Button buttonElez = new Button("Conferma Elezione");
		buttonElez.getElement().setAttribute("align", "center");

		final Button buttonInvio = new Button("Conferma lista");
		buttonInvio.getElement().setAttribute("align", "center");

		final Button buttonCand = new Button("Conferma Candidato");
		buttonCand.getElement().setAttribute("align", "center");


		vPanel.add(titolo);

		vPanel.add(new HTML("<br>"));
		vPanel.add(label0);
		vPanel.add(new HTML("<br>"));
		vPanel.add(Elez);
		vPanel.add(new HTML("<br>"));
		vPanel.add(buttonElez);

		vPanel.add(new HTML("<br>"));
		vPanel.add(label1);
		vPanel.add(new HTML("<br>"));
		vPanel.add(lista);
		vPanel.add(new HTML("<br>"));
		vPanel.add(buttonInvio);

		vPanel.add(new HTML("<br>"));
		vPanel.add(label2);
		vPanel.add(new HTML("<br>"));
		vPanel.add(listaCand);
		vPanel.add(new HTML("<br>"));
		vPanel.add(buttonCand);


		updateListboxElezione(Elez);


		buttonElez.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int indiceElez = Elez.getSelectedIndex();
				String nmElez = Elez.getValue(indiceElez );


				lista.clear();

				updateListboxLista(lista,nmElez);

			}
		});

		buttonInvio.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {


				int indiceLista = lista.getSelectedIndex();
				String nmLista = lista.getValue(indiceLista );
				int indiceEle = Elez.getSelectedIndex();
				String nmEle = Elez.getValue(indiceEle );

				ArrayList<String> datiSommario = new ArrayList<String>();
				datiSommario.add(nmLista);
				datiSommario.add(nmEle);



				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);


				greetingService.visualizzaElezioni(datiSommario, new AsyncCallback<String>() {

					public void onFailure(Throwable caught) {
						// Show the RPC error message to the user

						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText("ERRORE!!"+ caught.getMessage());

					}

					@SuppressWarnings("unused")
					@Override
					public void onSuccess(String result) {



						final MyDialogBox dialogBox = new MyDialogBox("=> "+result);

					}

				});	


				int indiceElez = Elez.getSelectedIndex();
				String nmElez = Elez.getValue(indiceElez );
				String lista = nmElez+"-"+nmLista;
				greetingService.getInfoLista(lista, new AsyncCallback<String>() {


					public void onFailure(Throwable caught) {
						// Show the RPC error message to the user

						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText("ERRORE!!"+ caught.getMessage());

					}
					@SuppressWarnings("unused")
					@Override
					public void onSuccess(String lista) {

						String [] risposta =lista.split("\n");

						listaCand.clear();	

						String[]  splittati = risposta[3].split("Candidati:");
						String cand  = splittati[1];
						String [] candidati = cand.split(";");
						listaCand.addItem("No preferenza");
						for(String x : candidati) {


							listaCand.addItem(x);
						}
					}
				});

			}

		});


		buttonCand.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int indiceLsCand = listaCand.getSelectedIndex();
				String nmCand = listaCand.getValue(indiceLsCand);

				int indiceLista = lista.getSelectedIndex();
				String nmLista = lista.getValue(indiceLista );

				int indiceElez = Elez.getSelectedIndex();
				String nmElez = Elez.getValue(indiceElez );


				ArrayList<String> candidatoScelto = new ArrayList<String>();
				candidatoScelto.add(nmCand);
				candidatoScelto.add(nmElez);


				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

				greetingService.contaVotiCand(candidatoScelto, new AsyncCallback<String>() {

					public void onFailure(Throwable caught) {
						// Show the RPC error message to the user

						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText("ERRORE!!"+ caught.getMessage());

					}
					@SuppressWarnings("unused")
					@Override

					public void onSuccess(String lista) {
						final MyDialogBox dialogBox = new MyDialogBox(""+lista);	


					}

				});

				lista.clear();
				listaCand.clear();
			}


		});


		vPanel.getElement().setAttribute("align", "center");
		RootPanel.get().add(vPanel);
	}

}
