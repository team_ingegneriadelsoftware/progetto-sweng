package progettosweng.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class MyDialogBox extends DialogBox  {

	private DialogBox mioDialogBox = null;

	/**
	 * Costruttore della classe MyDialogBox
	 * @param testo Testo da visualizzare
	 */
	public MyDialogBox(String testo){

		this.mioDialogBox = new DialogBox(false, true);
		this.mioDialogBox.setText("⚠️ Attenzione!");

		Label content = new Label(testo.replaceAll(" +", "\u00a0").replaceAll("\n", " "));
		content.setWidth("1%");

		if (this.mioDialogBox.isAutoHideEnabled())  {
			this.mioDialogBox.setWidget(content);
		} else {
			VerticalPanel vPanel = new VerticalPanel();
			vPanel.setSpacing(2);
			vPanel.add(content);vPanel.add(new Label("\n"));

			final Button close = new Button("Close");			
			close.addClickHandler(new ClickHandler() {

				public void onClick(ClickEvent event) {
					mioDialogBox.hide();
				}
			});
			close.getElement().setAttribute("align", "center");

			vPanel.add(close);
			this.mioDialogBox.setWidget(vPanel);
		}
		this.mioDialogBox.center();
		this.mioDialogBox.show();

	}
}