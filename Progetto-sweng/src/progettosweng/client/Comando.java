package progettosweng.client;

import com.google.gwt.user.client.Command;
import com.google.gwt.user.client.ui.VerticalPanel;


/**
 * Classe per gestire le diverse funzionalita' della pagina
 */
public class Comando {

	private VerticalPanel vPanel = null;
	
	/**
	 * Costruttore della classe Comando
	 * @param vp Vertical Panel
	 */

	public Comando(VerticalPanel vp) {

		this.vPanel = vp;

	}
	
	
	/**
	 * 
	 * @param tipo Tipo di Panel da creare: "login", "registrazione", "info", ...
	 * @return Oggetto contenente un Vertical Panel per il servizio richiesto
	 */
	public Command comando(String tipo) {

		Command cmd = null;


		if(tipo.equalsIgnoreCase("login")) {

			cmd = new Command() {

				public void execute() {

					vPanel.clear();

					Progetto_sweng main = new Progetto_sweng();
					main.onModuleLoad();


				}
			};
		}

		if(tipo.equalsIgnoreCase("registrazione")) {

			cmd = new Command() {

				public void execute() {

					vPanel.clear();
					GUI_Registrazione reg = new GUI_Registrazione(vPanel);
					reg.onModuleLoad();

				}
			};
		}

		if(tipo.equalsIgnoreCase("info")) {

			cmd = new Command() {

				public void execute() {

					vPanel.clear();
					GUI_Info info = new GUI_Info(vPanel);
					info.onModuleLoad();

				}
			};
		}


		if(tipo.equalsIgnoreCase("Visualizza report")) {

			cmd = new Command() {

				public void execute() {

					vPanel.clear();
					GUI_Report r = new GUI_Report(vPanel);
					r.onModuleLoad();


				}
			};
		}

		if(tipo.equalsIgnoreCase("Esprimi voto")) {

			cmd = new Command() {

				public void execute() {

					vPanel.clear();
					GUI_EsprimiVoto ev = new GUI_EsprimiVoto(vPanel);
					ev.onModuleLoad();


				}
			};
		}
		
		if(tipo.equalsIgnoreCase("Richiesta giornalista")) {

			cmd = new Command() {

				public void execute() {
					
					vPanel.clear();
					GUI_RichiestaGiornalista rg = new GUI_RichiestaGiornalista(vPanel);
					rg.onModuleLoad();
					
				}
			};
		}
		
		if(tipo.equalsIgnoreCase("Presenta lista")) {

			cmd = new Command() {

				public void execute() {
					
					vPanel.clear();
					GUI_PresentaLista pl = new GUI_PresentaLista(vPanel);
					pl.onModuleLoad();
					
				}
			};
		}
		
		if(tipo.equalsIgnoreCase("Elezioni Concluse")) {

			cmd = new Command() {

				public void execute() {
					
					vPanel.clear();
					GUI_ElezioniConcluse ec = new GUI_ElezioniConcluse(vPanel);
					ec.onModuleLoad();
					
				}
			};
		}
		
		if(tipo.equalsIgnoreCase("Nomina funzionario")) {
			
			cmd = new Command() {
				
				public void execute() {
					
					vPanel.clear();
					GUI_NominaFunzionario nf = new GUI_NominaFunzionario(vPanel);
					nf.onModuleLoad();
					
				}
			};
		}
		if(tipo.equalsIgnoreCase("Bandisci elezione")) {
			
			cmd = new Command() {
				
				public void execute() {
					
					vPanel.clear();
					GUI_BandisciElezione ec = new GUI_BandisciElezione(vPanel);
					ec.onModuleLoad();
					
				}
			};
		}
		

		if(tipo.equalsIgnoreCase("Richieste")) {
			
			cmd = new Command() {
				
				public void execute() {
					
					vPanel.clear();
					GUI_ApprovazioneRichieste arc = new GUI_ApprovazioneRichieste(vPanel);
					arc.onModuleLoad();
					
				}
			};
		}
//TO DO PROFILO (AMMINISTRATORE - GIORNALISTA - UTENTEREGISTRATO-FUNZIONARIOCOMUNALE)
		if(tipo.equalsIgnoreCase("Profilo")) {
			
			cmd = new Command() {
				
				public void execute() {
					
					vPanel.clear();
					
				
					switch(Passport.tipoAccount) {
					case "0":
						Progetto_sweng reg0 = new Progetto_sweng();
						reg0.onModuleLoad();
						break;
						
					case "1":
						GUI_FunzionarioComunale reg1 = new GUI_FunzionarioComunale(vPanel);
						reg1.onModuleLoad();
						break;

					case "2" :

						GUI_Amministratore reg2 = new GUI_Amministratore(vPanel);
						reg2.onModuleLoad();
						break;

					case "3" :
						
						GUI_UtenteRegistrato reg3 = new GUI_UtenteRegistrato(vPanel);
						reg3.onModuleLoad();
						break;
						
					case "4" :
						
						GUI_Giornalista reg4 = new GUI_Giornalista(vPanel);
						reg4.onModuleLoad();
						break;

					default:
						System.out.println("!!!!!!!!!!!!!!!!!!!Errore interno! >_<");
						break;

					}
					
				}
			};
		}

		return cmd;
	}

}
