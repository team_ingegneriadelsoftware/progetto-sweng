package progettosweng.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.TimeZone;
import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.Date;

public class GUI_Report {

	private VerticalPanel vPanel = null;
	
	/**
	 * Costruttore della classe GUI_Report
	 * @param vp VerticalPanel
	 */
	public GUI_Report(VerticalPanel vp) {
		this.vPanel = vp;

	}
	
	/**
	 * Metodo che consente di aggiornare la lista delle elezioni
	 * @param listbox ListBox delle elezioni
	 */
	public void updateListboxElezione(ListBox listbox) {

		Date date = new Date();

		DateTimeFormat dtf = DateTimeFormat.getFormat("yyyy-MM-dd hh:mm:ss");
		String data = dtf.format(date, TimeZone.createTimeZone(0));

		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getElezioniConcluse(data, new AsyncCallback < ArrayList<String> >() {


				public void onFailure(Throwable caught) {

					final DialogBox dialogBox = new DialogBox();
					dialogBox.setText("ERRORE!!"+ caught.getMessage());

				}
				@SuppressWarnings("unused")
				public void onSuccess( ArrayList<String> lista) {
					System.out.println(lista);


					for(String x : lista) {

						String[] split = x.split("\t");
						String nmElez = split[0];
						listbox.addItem(nmElez);
					}

				}

			});	
		}catch(Error e){};

	}

	/**
	 * Carica la GUI
	 */
	public void onModuleLoad() {

		HTML titolo = new HTML("<h1> Visualizza Report </h1>");

		vPanel.add(titolo);

		final Label label1 = new Label("Seleziona elezione ");

		ListBox elezione = new ListBox();

		Date date = new Date();

		DateTimeFormat dtf = DateTimeFormat.getFormat("yyyy-MM-dd hh:mm:ss");
		String data = dtf.format(date, TimeZone.createTimeZone(0));

		updateListboxElezione(elezione);

		final Button buttonInvio = new Button("Ottieni");
		buttonInvio.getElement().setAttribute("align", "center");


		final Grid grid = new Grid(4, 5);

		grid.setWidget(0, 0,  label1);
		grid.setWidget(0, 2, elezione);
		grid.setWidget(0, 4, buttonInvio);

		vPanel.add(grid);

		vPanel.add(new HTML("<hr>"));

		HorizontalPanel hPanel = new HorizontalPanel();
		vPanel.add(new HTML("<h1> Report </h1>"));


		buttonInvio.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {


				int index = elezione.getSelectedIndex();
				String selectedElezione = elezione.getValue(index);
				
				vPanel.add(hPanel);
				hPanel.clear();
				

				try {

					final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

					greetingService.getReport(selectedElezione, new AsyncCallback < ArrayList<String> >() {


						public void onFailure(Throwable caught) {

							final DialogBox dialogBox = new DialogBox();
							dialogBox.setText("ERRORE!!"+ caught.getMessage());

						}
						@SuppressWarnings("unused")
						public void onSuccess( ArrayList<String> lista) {
							
							String result = "";
							for(String i : lista) {
								result += (i+"<br>") ; 
							}

							hPanel.add(new HTML(result));
							hPanel.getElement().setAttribute("align", "left");

						}

					});	

				}catch(Error e){};

			}

		});


		vPanel.getElement().setAttribute("align", "center");
		RootPanel.get().add(vPanel);

	}
}
