package progettosweng.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DateBox;

public class GUI_BandisciElezione {

	private VerticalPanel vPanel = null;

	/**
	 * Costruttore dell classe GUI_BandisciElezione
	 * @param vp VerticalPanel
	 */
	public GUI_BandisciElezione ( VerticalPanel vp ) {

		this.vPanel = vp;

	}

	/**
	 * carica la GUI
	 */
	public void onModuleLoad() {


		HTML titolo = new HTML("<h1> Bandisci Elezione   </h1>");


		final Label lbNome = new Label("Nome ");
		final TextBox tbNome = new TextBox();

		final Label lbDataInizio = new Label("Data Inizio ");
		final DateBox tbDataInizio = new DateBox();
		tbDataInizio.setValue(new Date());

		final Label lbDataFine = new Label("Data Fine ");
		final DateBox tbDataFine = new DateBox();
		tbDataFine.setValue(new Date());

		final Button buttonInvio = new Button("Conferma dati");
		buttonInvio.getElement().setAttribute("align", "center");

		final Grid grid = new Grid(7, 7);

		grid.setWidget(0, 0,  lbNome);
		grid.setWidget(0, 1,  tbNome);
		grid.setWidget(1, 0,  lbDataInizio);
		grid.setWidget(1, 1,  tbDataInizio);
		grid.setWidget(2, 0,  lbDataFine);
		grid.setWidget(2, 1,  tbDataFine);


		buttonInvio.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {

				DateTimeFormat dateFormat =DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss");

				Date dataInizio = tbDataInizio.getValue();
				String dataInizioString  =dateFormat.format(dataInizio);

				Date dataFine = tbDataFine.getValue();
				String dataFineString=dateFormat.format(dataFine);

				ArrayList<String> datiElezione = new ArrayList<String>();

				datiElezione.add(tbNome.getText());
				datiElezione.add(dataInizioString);
				datiElezione.add(dataFineString);


				try {

					final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);


					greetingService.bandisciElezione(datiElezione,Passport.username, new AsyncCallback<String>() {

						public void onFailure(Throwable caught) {

							final DialogBox dialogBox = new DialogBox();
							dialogBox.setText("ERRORE!!"+ caught.getMessage());

						}

						@SuppressWarnings("unused")
						@Override
						public void onSuccess(String result) {

							final MyDialogBox dialogBox = new MyDialogBox(""+result);

						}

					});	
				}catch(Error e){};
			}

		});

		vPanel.add(new HTML("<br>"));
		vPanel.add(titolo);
		vPanel.add(new HTML("<br>"));
		vPanel.add(grid);
		vPanel.add(new HTML("<br>"));
		vPanel.add(buttonInvio);


		vPanel.getElement().setAttribute("align", "center");
		RootPanel.get().add(vPanel);

	}

}
