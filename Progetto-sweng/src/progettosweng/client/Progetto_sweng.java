package progettosweng.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Progetto_sweng implements EntryPoint {


	private final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	private VerticalPanel vPanel = null;

	/**
	 * Inizia l'applicazione
	 */
	public void onModuleLoad() {

		RootPanel.get().clear();

		vPanel = new VerticalPanel();
		vPanel.setHorizontalAlignment(HasHorizontalAlignment.ALIGN_CENTER);


		HTML titolo = new HTML("<h1> Elezioni Comunali </h1>");
		HTML operazione = new HTML("<h2> Login </h2>");

		final Label label1 = new Label("Inserire username: ");
		final TextBox textBoxUsername = new TextBox();
		final Label label2 = new Label("Inserire password: ");
		final TextBox textBoxPassword = new TextBox();

		final Grid grid = new Grid(2, 2);

		grid.setWidget(0, 0,  label1);
		grid.setWidget(0, 1,  textBoxUsername);
		grid.setWidget(1, 0,  label2);
		grid.setWidget(1, 1,  textBoxPassword);

		final Button buttonInvio = new Button("Avanti");
		buttonInvio.getElement().setAttribute("align", "center");

		buttonInvio.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

				greetingService.login(textBoxUsername.getText(), textBoxPassword.getText(), new AsyncCallback<String>() {

					public void onFailure(Throwable caught) {
						// Show the RPC error message to the user

						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText(caught.getMessage());

					}

					@Override
					public void onSuccess(String result) {

						Passport.username = (textBoxUsername.getText());

						Passport.tipoAccount = result;

						if(!result.equals("-1")) {

							vPanel.clear();

							switch(result) {
							case "0":
								Progetto_sweng reg0 = new Progetto_sweng();
								reg0.onModuleLoad();
								break;

							case "1":
								GUI_FunzionarioComunale reg1 = new GUI_FunzionarioComunale(vPanel);
								reg1.onModuleLoad();
								break;

							case "2" :

								GUI_Amministratore reg2 = new GUI_Amministratore(vPanel);
								reg2.onModuleLoad();
								break;

							case "3" :

								GUI_UtenteRegistrato reg3 = new GUI_UtenteRegistrato(vPanel);
								reg3.onModuleLoad();
								break;

							case "4" :

								GUI_Giornalista reg4 = new GUI_Giornalista(vPanel);
								reg4.onModuleLoad();
								break;

							default:
								System.out.println("[x]Errore interno!");
								break;

							}

						}else {

							final MyDialogBox dialogBox = new MyDialogBox("Utente o Password errata!!");

						}

					}

				});

			}

		});

		vPanel.add(titolo);
		vPanel.add(operazione);
		vPanel.add(new HTML("<br>"));
		vPanel.add(grid);
		vPanel.add(new HTML("<br>"));
		vPanel.add(buttonInvio);		

		Menu menu = new Menu( this.vPanel, 0);
		menu.onModuleLoad();


		vPanel.getElement().setAttribute("align", "center");

		RootPanel.get().add(vPanel);

	}

}
