package progettosweng.client;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.datepicker.client.DateBox;




public class GUI_Registrazione {

	private VerticalPanel vPanel = null;

	/**
	 * costruttore della classe GUI_Registrazione
	 * @param vp VerticalPanel
	 */
	public GUI_Registrazione( VerticalPanel vp) {

		this.vPanel = vp;

	}
	
	/**
	 * carica la GUI
	 */
	public void onModuleLoad() {

		Menu menu = new Menu( this.vPanel, 0);
		menu.onModuleLoad();

		HTML titolo = new HTML("<h1> Elezioni Comunali </h1>");
		HTML operazione = new HTML("<h2> Registrazione </h2>");

		final Label label1 = new Label("Inserire username: ");
		final TextBox textBoxUsername = new TextBox();
		final Label label2 = new Label("Inserire password: ");
		final TextBox textBoxPassword = new TextBox();
		final Label label3 = new Label("Inserire nome: ");
		final TextBox textBoxNome = new TextBox();
		final Label label4 = new Label("Inserire cognome: ");
		final TextBox textBoxCognome = new TextBox();
		final Label label5 = new Label("Inserire telefono: ");
		final TextBox textBoxTelefono = new TextBox();
		final Label label6 = new Label("Inserire email: ");
		final TextBox textBoxEmail = new TextBox();
		final Label label7 = new Label("Inserire Codice Fiscale: ");
		final TextBox textBoxCF = new TextBox();
		final Label label8 = new Label("Inserire indirizzo: ");
		final TextBox textBoxIndirizzo = new TextBox();
		final Label label9 = new Label("Inserire Data di Nascita: ");
		final DateBox textBoxDataNascita = new DateBox();
		textBoxDataNascita.setValue(new Date());


		final Label label10 = new Label("Tipo di documento: ");
		final ListBox dropBox = new ListBox();
		dropBox.addItem("Carta Identita'");
		dropBox.addItem("Passaporto");

		final Label label11 = new Label("Numero di documento: ");
		final TextBox textBoxNumeroDocumento= new TextBox();
		final Label label12 = new Label("Rilasciato da: ");
		final TextBox textBoxRilasciatoDa= new TextBox();
		final Label label13 = new Label("Data di Rilascio: ");
		final DateBox textBoxDataRilascio= new DateBox();
		textBoxDataRilascio.setValue(new Date());
		final Label label14 = new Label("Data di Scandenza: ");
		final DateBox textBoxDataScadenza= new DateBox();
		textBoxDataScadenza.setValue(new Date());


		final ArrayList<Widget> listaWidgets = new ArrayList<Widget>();

		Collections.addAll(listaWidgets, label1,textBoxUsername,label2,textBoxPassword,
				label3, textBoxNome,label4,textBoxCognome,label5, textBoxTelefono, label6,
				textBoxEmail,label7, textBoxCF,label8,textBoxIndirizzo, label9, textBoxDataNascita,
				label10,dropBox, label11, textBoxNumeroDocumento, label12,
				textBoxRilasciatoDa,label13,textBoxDataRilascio, label14, textBoxDataScadenza  );

		final Grid grid = new Grid(14, 2);
		int posizione = 0;

		for (int row = 0; row < 14; row++) {
			for (int col = 0; col < 2; col++) {
				grid.setWidget(row, col, listaWidgets.get(posizione) );
				posizione++;
			}
		}

		final Button buttonInvio = new Button("Avanti");
		buttonInvio.getElement().setAttribute("align", "center");
		buttonInvio.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {


				DateTimeFormat dateFormat =DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss");

				Date data = textBoxDataNascita.getValue();
				String dateString=dateFormat.format(data);

				Date dataR = textBoxDataRilascio.getValue();
				String StringDataRilascio  =dateFormat.format(dataR);

				Date dataS = textBoxDataScadenza.getValue();
				String StringDataScad  =dateFormat.format(dataS);

				ArrayList<String> listaDati = new ArrayList<String>();

				listaDati.add(textBoxUsername.getText());
				listaDati.add(textBoxPassword.getText());
				listaDati.add(textBoxNome.getText());
				listaDati.add(textBoxCognome.getText());
				listaDati.add(textBoxTelefono.getText());
				listaDati.add(textBoxEmail.getText());
				listaDati.add(textBoxCF.getText());
				listaDati.add(textBoxIndirizzo.getText());
				listaDati.add(dateString);


				listaDati.add(dropBox.getSelectedValue());

				listaDati.add(textBoxNumeroDocumento.getText());
				listaDati.add(textBoxRilasciatoDa.getText());
				listaDati.add(StringDataRilascio);
				listaDati.add(StringDataScad);



				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);


				greetingService.registrazione(listaDati, new AsyncCallback<String>() {

					public void onFailure(Throwable caught) {

						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText("ERRORE!!"+ caught.getMessage());

					}

					@SuppressWarnings("unused")
					@Override
					public void onSuccess(String result) {

						if(result.equalsIgnoreCase("ok")) {

							final MyDialogBox dialogBox = new MyDialogBox("=> "+result);

							vPanel.clear();
							Progetto_sweng reg = new Progetto_sweng();
							reg.onModuleLoad();

						}else {

							final MyDialogBox dialogBox = new MyDialogBox("=> "+result);

						}
					}

				});	

			}

		});


		vPanel.add(titolo);
		vPanel.add(operazione);
		vPanel.add(new HTML("<br>"));
		vPanel.add(grid);
		vPanel.add(new HTML("<br>"));
		vPanel.add(buttonInvio);
		vPanel.getElement().setAttribute("align", "center");


	}

}
