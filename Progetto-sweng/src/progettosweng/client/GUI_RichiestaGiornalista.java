package progettosweng.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class GUI_RichiestaGiornalista {

	private VerticalPanel vPanel = null;
	
	/**
	 * costruttore della classe GUI_RichiestaGiornalista
	 * @param vp VerticalPanel
	 */
	public GUI_RichiestaGiornalista( VerticalPanel vp ) {

		this.vPanel = vp;

	}
	
	/**
	 * carica la GUI
	 */
	public void onModuleLoad() {

		HTML titolo = new HTML("<h1> Richiesta accreditamento giornalista</h1>"
				+ "<br><h3>Registrati come giornalista e avrai la possibilita' <br> di seguire le elezioni da vicino </h3>");


		final Button buttonInvioRichiesta = new Button("InviaRichiesta");
		buttonInvioRichiesta.getElement().setAttribute("align", "center");



		buttonInvioRichiesta.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				try {
					final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

					greetingService.RichiestaAccreditamentoGiornalista(Passport.username,new AsyncCallback< String >() {

						public void onFailure(Throwable caught) {

							final DialogBox dialogBox = new DialogBox();
							dialogBox.setText(caught.getMessage());
						}

						@Override
						public void onSuccess( String risposta) {

							final MyDialogBox dialogBox = new MyDialogBox("=> "+risposta);

						}

					});
				}catch(Error e){};
			}

		});

		vPanel.add(titolo);
		vPanel.add(new HTML("<br>"));
		vPanel.add(new HTML("<br>"));
		vPanel.add(buttonInvioRichiesta);
		vPanel.getElement().setAttribute("align", "center");
		RootPanel.get().add(vPanel);


	}

}
