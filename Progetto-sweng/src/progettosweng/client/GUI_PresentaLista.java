package progettosweng.client;

import java.util.*;
import progettosweng.shared.*;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class GUI_PresentaLista {


	private VerticalPanel vPanel = null;


	public GUI_PresentaLista( VerticalPanel vp ) {

		this.vPanel = vp;

	}

	public void updateListboxUtentiRegistrati(ListBox listbox) {
		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getCandidatiDisponibili(new AsyncCallback <ArrayList<String>>() {

				public void onFailure(Throwable caught) {

					final DialogBox dialogBox = new DialogBox();
					dialogBox.setText(caught.getMessage());
				}

				public void onSuccess(ArrayList<String> lista) {


					listbox.clear();
					for(String x : lista) {
						if(x != null) {
							listbox.addItem(x);
						}

					}

				}

			});

		}catch(Error e){};
	}
	public void updateListboxElezione(ListBox listbox) {
		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getElezioneInCorso(new AsyncCallback < ArrayList <String> >() {


				public void onFailure(Throwable caught) {

					final DialogBox dialogBox = new DialogBox();
					dialogBox.setText("ERRORE!!"+ caught.getMessage());

				}
				@SuppressWarnings("unused")
				@Override
				public void onSuccess( ArrayList<String> lista) {

					for(String x : lista) {
						listbox.addItem(x);
					}

				}

			});
		}catch(Error e){};

	}

	String Candidati = "";
	ArrayList<Integer> controlloIndici = new ArrayList<Integer>();


	public void onModuleLoad() {
		Candidati = "";
		controlloIndici.clear();


		HTML titolo = new HTML("<h1> Presenta Lista  </h1>");


		final Label lbElezione = new Label("Elezione ");
		final ListBox elezione = new ListBox();



		final Label lbNome = new Label("Nome ");
		final TextBox tbNome = new TextBox();

		final Label lbSimbolo = new Label("Simbolo: ");
		final TextBox tbSimbolo = new TextBox();


		final Label lbSindaco = new Label("Sindaco");
		final ListBox sindaco = new ListBox();

		final Label lbCand = new Label("Candidato");
		final ListBox candidato = new ListBox();



		final Button buttonInvio = new Button("Invia lista");
		buttonInvio.getElement().setAttribute("align", "center");

		final Button buttonAdd = new Button("Add");
		buttonAdd.getElement().setAttribute("align", "center");

		final Button buttonReset = new Button("Reset");
		buttonReset.getElement().setAttribute("align", "center");


		final Grid grid = new Grid(7, 7);

		grid.setWidget(0, 0, lbElezione);
		grid.setWidget(0, 1, elezione);
		grid.setWidget(1, 0, lbNome);
		grid.setWidget(1, 1, tbNome);
		grid.setWidget(2, 0, lbSindaco);
		grid.setWidget(2, 1, sindaco);
		grid.setWidget(3, 0, lbSimbolo);
		grid.setWidget(3, 1, tbSimbolo);

		grid.setWidget(4, 0, lbCand);
		grid.setWidget(4, 1, candidato);
		grid.setWidget(4, 2, buttonAdd);

		grid.setWidget(6, 2, buttonReset);
		grid.setWidget(6, 1, buttonInvio);



		updateListboxUtentiRegistrati(sindaco);
		updateListboxUtentiRegistrati(candidato);
		updateListboxElezione(elezione);


		buttonAdd.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				int controllo = 0;

				int index = candidato.getSelectedIndex();
				String Cand = candidato.getValue(index);
				for(int i : controlloIndici) {
					if(i == index) {
						controllo = 1;
						final MyDialogBox dialogBox = new MyDialogBox("Candidato: "+Cand+" gia' selezionato");

					}

				}
				if(controllo == 0) {
					controlloIndici.add(index);
					Candidati += "\t"+Cand;

					final MyDialogBox dialogBox = new MyDialogBox("Candidati selezionati : \n"+Candidati);

				}

			}

		}
				);

		buttonReset.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				Candidati = "";
				controlloIndici.clear();

				tbNome.setText(null);
				tbSimbolo.setText(null);

			}

		}
				);

		buttonInvio.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				int indiceElez = elezione.getSelectedIndex();
				String nmElez = elezione.getValue(indiceElez );

				int indiceSind = sindaco.getSelectedIndex();
				String nmSindaco = sindaco.getValue(indiceSind);

				ArrayList<String> datiLista = new ArrayList<String>();

				if(tbNome.getText() != null && tbSimbolo.getText()!= null && Candidati != "") {

					datiLista.add(nmElez);
					datiLista.add(tbNome.getText());
					datiLista.add(tbSimbolo.getText());
					datiLista.add(nmSindaco);
					datiLista.add(Candidati);
					final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

					greetingService.presentaLista(datiLista,Passport.username, new AsyncCallback<String>() {


						public void onFailure(Throwable caught) {
							// Show the RPC error message to the user

							final DialogBox dialogBox = new DialogBox();
							dialogBox.setText("ERRORE!!"+ caught.getMessage());

						}
						@SuppressWarnings("unused")
						@Override
						public void onSuccess(String result) {

							tbSimbolo.setText(null);
							tbNome.setText(null);

							final MyDialogBox dialogBox = new MyDialogBox(result);
							updateListboxUtentiRegistrati(sindaco);
							updateListboxUtentiRegistrati(candidato);

						}

					});

				}else {

					final MyDialogBox dialogBox = new MyDialogBox("Errore : Inserire tutti i campi");

				}
			}

		});


		vPanel.add(new HTML("<br>"));
		vPanel.add(titolo);
		vPanel.add(new HTML("<br>"));
		vPanel.add(grid);
		vPanel.getElement().setAttribute("align", "center");
		RootPanel.get().add(vPanel);

	}

}