package progettosweng.client;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.AsyncCallback;

import progettosweng.shared.*;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {
	/**
	 * Metodo per consentire la registrazione da parte di un nuovo Utente
	 * @param listaDati Dati dell'utente che intende registrarsi
	 * @return Esito della richiesta di registrazione
	 * @throws IllegalArgumentException
	 */
	void registrazione( ArrayList<String> listaDati,
			AsyncCallback<String> callback) throws IllegalArgumentException;
	/**
	 * Metodo per consentire il Login da parte di un utente registrato
	 * @param username username dell'utente registrato
	 * @param password password dell'utente registrato
	 * @return esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	void login( String username, String password,
			AsyncCallback<String> callback) throws IllegalArgumentException;
	/**
	 * Metodo che ritorna tutti gli utenti registrati
	 * @return Lista di utente registrati
	 * @throws IllegalArgumentException
	 */
	void getUtentiRegistrati(AsyncCallback< ArrayList<String> > callback) throws IllegalArgumentException;
	/**
	 * Metodo per bandire una determinata Elezione
	 * @param datiElezione I dati della Elezione
	 * @param username Username del Funzionario Comunale che bandisce l'elezione
	 * @return
	 * @throws IllegalArgumentException
	 */
	void bandisciElezione( ArrayList<String> datiElezione,String username, AsyncCallback<String> callback) throws IllegalArgumentException;
	/**
	 * Metodo che permette di inviare una richiesta per essere accreditato come giornalista
	 * 
	 * @param username Username del giornalista
	 * @return esito della richiesta
	 * @throws IllegalArgumentException
	 */
	void RichiestaAccreditamentoGiornalista(String username,AsyncCallback<String> callback) throws IllegalArgumentException;
	/**
	 * 
	 * Metodo che ritorna tutte le richieste dei giornalisti
	 * @return Lista degli usernames dei giornalisti
	 * @throws IllegalArgumentException
	 */
	void getListaRichiesteGiornalista(AsyncCallback< ArrayList<String> > callback) throws IllegalArgumentException;
	/**
	 * 
	 * Metodo che consente di presentare una lista
	 * 
	 * @param datiLista Dati della lista corrente
	 * @param usernameRichiedente Username del richiedente
	 * @return Esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	void presentaLista( ArrayList<String> datiLista,String usernameRichiedente, AsyncCallback<String> callback) throws IllegalArgumentException;
	/**
	 * Metodo che consente di approvare la richiesta di un giornalista
	 * @param username Username dell'utente da accreditare
	 * @return L'esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	void setApprovaRichiestaGiornalista(String username,AsyncCallback<String> callback)throws IllegalArgumentException;	
	/**
	 *  Metodo che consente di rifiutare la richiesta di un giornalista
	 * @param username Username dell'utente da rifiutare
	 * @return L'esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	void setRifiutaRichiestaGiornalista(String username, AsyncCallback<String> callback)throws IllegalArgumentException;
	/**
	 * Metodo che fornisce le informazioni di un determinato utente registrato
	 * @param username Username dell'utente registrato
	 * @return Le informazioni dell'utente richiesto
	 * @throws IllegalArgumentException
	 */
	void getInfoUsername(String username, AsyncCallback<String> callback)throws IllegalArgumentException;
	/**
	 * Metodo che ritorna tutte le liste elettorali
	 * @return le liste elettorali presenti nel sistema
	 * @throws IllegalArgumentException
	 */
	void getListaElettorale(AsyncCallback< ArrayList<String> > callback) throws IllegalArgumentException;
	/**
	 * Metodo che ritorna i dati delle elezioni in corso
	 * @return Una lista dei dati delle elezioni in corso
	 * @throws IllegalArgumentException
	 */
	void getElezioneInCorso(AsyncCallback< ArrayList<String> > callback) throws IllegalArgumentException;
	/**
	 * Metodo che ritorna i dati di tutte le elezioni concluse entro una determinata data.
	 * @param data La data da utilizzare per ricavare le elezioni concluse
	 * @return i dati delle elezioni concluse
	 * @throws IllegalArgumentException
	 */
	void getElezioniConcluse(String data,AsyncCallback< ArrayList<String> > callback) throws IllegalArgumentException;
	/**
	 * Metodo che restituisce l'informazione di una determinanta lista
	 * @param lista Nome della lista
	 * @return Informazione della lista richiesta
	 * @throws IllegalArgumentException
	 */
	void getInfoLista(String lista,AsyncCallback<String> callback) throws IllegalArgumentException;
	void setRichiestaLista(String lista,int tipo,String username, AsyncCallback<String> callback);
	/**
	 * Metodo che ritorna tutti i candidati disponibili
	 * @return lista di candidati
	 * @throws IllegalArgumentException
	 */
	void getCandidatiDisponibili(AsyncCallback<ArrayList<String>> callback) throws IllegalArgumentException;
	/**
	 * Metodo per ottenere tutti i sindaci presenti nel sistema
	 * @return lista di sindaci
	 * @throws IllegalArgumentException
	 */
	void getSindaci(AsyncCallback<ArrayList<String>> callback) throws IllegalArgumentException;
	/**
	 * metodo che restituisce gli utenti che non sono sindaci
	 * @return lista di utenti non sindaci
	 * @throws IllegalArgumentException
	 */
	void getUtentiNotSindaci(AsyncCallback<ArrayList<String>> callback)throws IllegalArgumentException;
	
	void esprimiVoto( ArrayList<String> datiVoto, AsyncCallback<String> callback) throws IllegalArgumentException;
	
	void visualizzaElezioni( ArrayList<String> datiVoto, AsyncCallback<String> callback) throws IllegalArgumentException;
	
	void contaVotiCand( ArrayList<String> candidatoScelto, AsyncCallback<String> callback) throws IllegalArgumentException;
	/**
	 * Metodo che restituisce l'informazione di un determinanto utente registrato
	 * @param username Username dell'utente registrato
	 * @return Le informazioni dell'utente registrato
	 * @throws IllegalArgumentException
	 */
	void getProfiloUtente(String username,AsyncCallback<String> callback)throws IllegalArgumentException;
	/**
	 * Metodo che restituisce tutte le richieste fatte da un utente
	 * @param username Username dell'utente registrato
	 * @return lista contenente i dati delle richieste effettuate
	 * @throws IllegalArgumentException
	 */
	void getRichiesteUtente(String username, AsyncCallback<ArrayList<String>> callback);
	/**
	 * Metodo che consente di nominare un Funzionario
	 * @param username username dell'utente registrato
	 * @return Esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	void nominaFunzionario(String username, AsyncCallback<String> callback)throws IllegalArgumentException;
	/**
	 * Metodo che ritorna tutti gli utenti registrati che non sono ne' Amministratori ne' Funzionari comunali
	 * @return Una lista contenente gli username degli utenti registrati presenti nel sistema
	 * @throws IllegalArgumentException
	 */
	void getUtentiRegistrati2(AsyncCallback< ArrayList<String> > callback) throws IllegalArgumentException;
	/**
	 * Metodo che restituisce tutte le richieste di registrazione
	 * @return Lista contenente le richieste di registrazione.
	 * @throws IllegalArgumentException
	 */
	void getRichiesteRegistrazione(AsyncCallback< ArrayList<String> > callback) throws IllegalArgumentException;
	/**
	 * Metodo che rifiuta  una richiesta di registrazione
	 * @param username username dell'utente che ha inviato la richiesta
	 * @return L'esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	void RifiutaRichiestaRegistrazione(String username, AsyncCallback<String> callback)throws IllegalArgumentException;
	/**
	 * Metodo che approva una richiesta di registrazione
	 * @param username Username dell'utente che ha inviato la richiesta di registrazione
	 * @return L'esito dell'operazione
	 * @throws IllegalArgumentException
	 */
	void ApprovaRichiestaRegistrazione(String username, AsyncCallback<String> callback)throws IllegalArgumentException;
	
	void getReport(String idReport, AsyncCallback< ArrayList<String>> callback)throws IllegalArgumentException;
	/**
	 * Metodo che ritorna tutte le liste elettorali approvate
	 * @return Una collezione delle liste elettorali che sono state approvate
	 * @throws IllegalArgumentException
	 */
	void getListaElettoraleApprovata(AsyncCallback<ArrayList<String>> callback);
	
}



