package progettosweng.client;

import java.util.ArrayList;

import com.gargoylesoftware.htmlunit.javascript.host.Console;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HasHorizontalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

import progettosweng.shared.UtenteRegistrato;

public class GUI_Amministratore {


	private VerticalPanel vPanel = null;

	/**
	 * Costruttore della classe GUI_Amministratore
	 * @param vp Vertical Panel
	 */
	public GUI_Amministratore( VerticalPanel vp) {

		this.vPanel = vp;

	}
	
	/**
	 * Carica la GUI
	 */
	public void onModuleLoad() {

		Menu menu = new Menu( this.vPanel, 2);
		menu.onModuleLoad();
		
		
		HTML titolo = new HTML("<h1> Amministratore </h1>");
		
		vPanel.add(new HTML("<br>"));
		vPanel.add(titolo);
		
		HorizontalPanel hPanel = new HorizontalPanel();
		
		
		
		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getProfiloUtente(Passport.username,new AsyncCallback<String>() {

			public void onFailure(Throwable caught) {

			final DialogBox dialogBox = new DialogBox();
			dialogBox.setText(caught.getMessage());
			}

			public void onSuccess(String info) {
				String [] arrayInfo = info.split("\n");
				
				String htmlProfilo = "<h2>Profilo</h2>"+
								arrayInfo[0]+"<br>"+
								arrayInfo[1]+"<br>"+
								arrayInfo[2]+"<br>"+
								arrayInfo[3]+"<br>"+
								arrayInfo[4]+"<br>"+
								arrayInfo[5]+"<br>"+
								arrayInfo[6]+"<br>"+
								arrayInfo[7]+"<br>";
		
				String htmlDocumento = "<h2>Documento</h2>"+
						arrayInfo[8]+"<br>"+
						arrayInfo[9]+"<br>"+
						arrayInfo[10]+"<br>"+
						arrayInfo[11]+"<br>"+
						arrayInfo[12];
				
				hPanel.add(new HTML(htmlProfilo));
				hPanel.add(new HTML("&emsp;&emsp;&emsp;&emsp;"));
				hPanel.add(new HTML(htmlDocumento));
				
				vPanel.getElement().setAttribute("align", "center");
				
				vPanel.add(hPanel);

				
			}

			});

		}catch(Error e){};
		
		
		try {
			
		final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

		greetingService.getRichiesteUtente(Passport.username,new AsyncCallback<ArrayList<String>>() {
			
			
		public void onFailure(Throwable caught) {

		final DialogBox dialogBox = new DialogBox();
		dialogBox.setText(caught.getMessage());
		}

		public void onSuccess(ArrayList <String> Richieste) {
			
			System.out.println("Username  =>"+Passport.username);
			
			String htmlListe = "";
			String buffer = "";
			
			for(String i : Richieste) {
				
				buffer += "<br>"+i;
						 
			
			htmlListe = buffer;
			
			}
			
	
			vPanel.add(new HTML("<hr>"));
			vPanel.add(new HTML("<h2>Richieste Effettuate : </h2>"));
			vPanel.add(new HTML(htmlListe));
			
			
		}

		});

	}catch(Error e){};
		
		

		
	}// fine onModuleLoad
 
}
