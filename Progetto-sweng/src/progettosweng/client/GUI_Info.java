package progettosweng.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class GUI_Info implements EntryPoint{

	private VerticalPanel vPanel = null;

	
	/**
	 * Costruttore della classe GUI_Info
	 * @param vp VerticalPanel
	 */
	public GUI_Info( VerticalPanel vp) {

		this.vPanel = vp;

	}
	/**
	 * carica la GUI
	 */

	@Override
	public void onModuleLoad() {

		HorizontalPanel hPanel = new HorizontalPanel();


		HTML titolo = new HTML("<h1> INFO </h1><h2> Versione 1.0 </h2>");
		HTML descrizione = new HTML(
				"<p> Elezioni comunali : e' un sistema  di voto online che permetta ai cittadini di:<br>" + 
						"● Registrarsi al sistema<br>" + 
						"● Indire una elezione (solo per i “Funzionari Comunali”)<br>" + 
						"● Presentare una lista elettorale per una elezione<br>" + 
						"● Votare una lista e un candidato<br>" + 
				"● Consultare i risultati di una elezione </p>");

		HTML membri = new HTML(" <style>div.container {display:inline-block;}p {text-align:center;}</style>"
				+"<h2>Team:</h2><br>"

		+ "<div class=\"container\">" + 

		"    <img src=\"https://source.unsplash.com/120x100/?night\" />\n" + 

		"    <p>Giulia Guidi</p>" + 

		"  </div>" + 

		"  <div class=\"container\">" + 

		"    <img class=\"middle-img\"src=\"https://source.unsplash.com/120x100/?city\" />" + 

		"    <p>Federica La Piana</p></div>" + 

		"  <div class=\"container\">" + 

		"    <img src=\"https://source.unsplash.com/120x100/?random\" />" + 

		"    <p>Renzo Huallparimachi</p>" + 

		"  </div>" + 

		"  <div class=\"container\">" + 

		"    <img src=\"https://source.unsplash.com/120x100/?flowers\"/>" + 

		"    <p>Miriana Rebeggiani</p>" + 

		"  </div>" + 

				"</div>");


		vPanel.add(titolo);
		hPanel.add(descrizione);
		
		vPanel.add(hPanel);
		vPanel.add(new HTML("<br><hr>"));
		vPanel.add(membri);

		hPanel.getElement().setAttribute("align", "left");
		vPanel.getElement().setAttribute("align", "center");

	}

}

