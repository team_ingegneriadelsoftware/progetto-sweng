package progettosweng.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class GUI_ApprovazioneRichieste {
	
	private VerticalPanel vPanel = null;


	final ListBox listaRichieste = new ListBox();


	public GUI_ApprovazioneRichieste( VerticalPanel vp ) {

		this.vPanel = vp;

	}


	/**
	 * Aggiorna la ListBox delle liste elettorali
	 * @param listbox ListBox
	 */
	public void updateListboxListaElettorale(ListBox listbox) {
		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getListaElettorale(new AsyncCallback  <ArrayList<String>>() {

				public void onFailure(Throwable caught) {

					final DialogBox dialogBox = new DialogBox();
					dialogBox.setText(caught.getMessage());
				}

				public void onSuccess(ArrayList<String> lista) {


					listbox.clear();

					for(String x : lista) {
						String [] splitPresentaLista = x.split("\t");

						String stato = splitPresentaLista[2];
						if(stato.equalsIgnoreCase("Pendente")) {

							String nomeElezione = splitPresentaLista[4];
							String nomeLista =splitPresentaLista[0];

							listbox.addItem(nomeElezione+"-"+nomeLista);

						}


					}

				}

			});

		}catch(Error e){};
	}



	/**
	 * Aggiorna la ListBox degli utente registrati
	 * @param listbox ListBox
	 */
	public void updateListboxUtentiRegistrati(ListBox listbox) {
		try {
			final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

			greetingService.getListaRichiesteGiornalista(new AsyncCallback  <ArrayList<String>>() {

				public void onFailure(Throwable caught) {

					final DialogBox dialogBox = new DialogBox();
					dialogBox.setText(caught.getMessage());
				}

				public void onSuccess(ArrayList<String> lista) {


					listbox.clear();
					for(String x : lista) {
						if(x != null) {
							listbox.addItem(x);
						}

					}

				}

			});

		}catch(Error e){};
	}
	
	/**
	 * Carica la GUI
	 */

	public void onModuleLoad() {


		HTML titolo = new HTML("<h1> Approvazione richiesta giornalista  </h1>");

		final Label Richiestagiornalista = new Label("Richiesta giornalista :  ");
		final ListBox giornalista = new ListBox();


		updateListboxUtentiRegistrati(giornalista);


		final Button buttonApprova = new Button("Approva");
		buttonApprova.getElement().setAttribute("align", "right");


		buttonApprova.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				try {
					final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
					int indiceItemListbox = giornalista.getSelectedIndex();
					String username = giornalista.getValue(indiceItemListbox);

					greetingService.setApprovaRichiestaGiornalista(username,new AsyncCallback <String>() {

						public void onFailure(Throwable caught) {

							final DialogBox dialogBox = new DialogBox();
							dialogBox.setText(caught.getMessage());
						}

						public void onSuccess(String risposta) {
							final MyDialogBox dialogBox = new MyDialogBox("=> "+risposta);


							updateListboxUtentiRegistrati(giornalista);



						}

					});
				}catch(Error e){};
			}

		});






		final Button buttonInfo = new Button("Info");
		buttonInfo.getElement().setAttribute("align", "center");

		buttonInfo.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
				int indiceItemListbox = giornalista.getSelectedIndex();
				String username = giornalista.getValue(indiceItemListbox);

				greetingService.getInfoUsername(username,new AsyncCallback <String>() {

					public void onFailure(Throwable caught) {

						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText(caught.getMessage());
					}

					public void onSuccess(String risposta) {
						final MyDialogBox dialogBox = new MyDialogBox(""+risposta);


					}

				});

			}

		});


		final Button buttonRifiuta = new Button("Rifiuta ");
		buttonRifiuta.getElement().setAttribute("align", "center");

		buttonRifiuta.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
				int indiceItemListbox = giornalista.getSelectedIndex();
				String username = giornalista.getValue(indiceItemListbox);

				greetingService.setRifiutaRichiestaGiornalista(username,new AsyncCallback <String>() {

					public void onFailure(Throwable caught) {

						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText(caught.getMessage());
					}

					public void onSuccess(String risposta) {
						final MyDialogBox dialogBox = new MyDialogBox("=> "+risposta);


						updateListboxUtentiRegistrati(giornalista);



					}

				});

			}

		});





		final Grid grid = new Grid(4, 4);

		grid.setWidget(0, 0,  Richiestagiornalista);
		grid.setWidget(0, 1,  giornalista);
		grid.setWidget(0, 3,  buttonInfo);
		grid.setWidget(2, 2,  buttonApprova);
		grid.setWidget(2, 1,  buttonRifiuta);




		// agg. al panel
		vPanel.add(new HTML("<br>"));
		vPanel.add(titolo);		
		vPanel.add(grid);
		vPanel.add(new HTML("<br>"));



		HTML titolo1 = new HTML("<h1> Approvazione richiesta lista elettorale  </h1>");



		final Label RichiestaLista = new Label("Richiesta lista :  ");
		final ListBox listaelettorale = new ListBox();



		updateListboxListaElettorale(listaelettorale);


		final Grid gridbuttonLista = new Grid(5,5);


		final Button buttonApprovaLista = new Button("Approva ");
		buttonApprovaLista.getElement().setAttribute("align", "center");

		buttonApprovaLista.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
				int indiceItemListbox = listaelettorale.getSelectedIndex();
				String lista = listaelettorale.getValue(indiceItemListbox);

				greetingService.setRichiestaLista(lista,0,Passport.username,new AsyncCallback <String>() {

					public void onFailure(Throwable caught) {

						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText(caught.getMessage());
					}

					public void onSuccess(String risposta) {
						final MyDialogBox dialogBox = new MyDialogBox("=> "+risposta);


						updateListboxListaElettorale(listaelettorale);


					}

				});


			}

		});


		final Button buttonRifiutaLista = new Button("Rifiuta ");
		buttonRifiutaLista.getElement().setAttribute("align", "center");

		buttonRifiutaLista.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
				int indiceItemListbox = listaelettorale.getSelectedIndex();
				String lista = listaelettorale.getValue(indiceItemListbox);

				greetingService.setRichiestaLista(lista,1,Passport.username,new AsyncCallback <String>() {

					public void onFailure(Throwable caught) {

						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText(caught.getMessage());
					}

					public void onSuccess(String risposta) {
						final MyDialogBox dialogBox = new MyDialogBox("=> "+risposta);


						updateListboxListaElettorale(listaelettorale);



					}

				});



			}

		});

		final Button infoLista = new Button("Info");
		infoLista.getElement().setAttribute("align", "center");

		infoLista.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {


				int indiceFirstLab = listaelettorale.getSelectedIndex();
				String lista = listaelettorale.getValue(indiceFirstLab);



				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

				greetingService.getInfoLista(lista, new AsyncCallback<String>() {


					public void onFailure(Throwable caught) {
						// Show the RPC error message to the user

						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText("ERRORE!!"+ caught.getMessage());

					}
					@SuppressWarnings("unused")
					@Override
					public void onSuccess(String lista) {



						final MyDialogBox dialogBox = new MyDialogBox(""+lista);

					}

				});	





			}

		});
		gridbuttonLista.setWidget(0, 1,  listaelettorale);
		gridbuttonLista.setWidget(0, 0,  RichiestaLista);

		gridbuttonLista.setWidget(0, 3,  infoLista);
		gridbuttonLista.setWidget(2, 1,  buttonRifiutaLista);
		gridbuttonLista.setWidget(2, 2,  buttonApprovaLista);




	
		vPanel.add(new HTML("<br>"));
		vPanel.add(titolo1);		
		vPanel.add(new HTML("<br>"));
		vPanel.add(gridbuttonLista);



		getRichiesteRegistrazione();

		HTML titolo2 = new HTML("<h1> Approvazione richiesta Registrazione </h1>");



		final Label RichiestaRegistrazione = new Label("Richiesta di Registrazione :  ");

		final Button buttonApprova2 = new Button("Approva");
		buttonApprova2.getElement().setAttribute("align", "right");

		buttonApprova2.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				
				int indiceSelezionato = listaRichieste.getSelectedIndex();
				
				String username = (listaRichieste.getItemText(indiceSelezionato).split(":"))[0].trim();

				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
				greetingService.ApprovaRichiestaRegistrazione(username,new AsyncCallback <String>() {

					public void onFailure(Throwable caught) {

						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText(caught.getMessage());
					}

					public void onSuccess(String risposta) {

						MyDialogBox myDialogBox = new MyDialogBox(risposta);
						
						listaRichieste.removeItem(indiceSelezionato);
					}

				});

			}

		});

		final Button buttonRifiuta2 = new Button("Rifiuta ");
		buttonRifiuta2.getElement().setAttribute("align", "center");

		buttonRifiuta2.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				int indiceSelezionato = listaRichieste.getSelectedIndex();
				String username = (listaRichieste.getItemText(indiceSelezionato).split(":"))[0].trim();

				
				final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
				greetingService.RifiutaRichiestaRegistrazione(username,new AsyncCallback <String>() {

					public void onFailure(Throwable caught) {

						final DialogBox dialogBox = new DialogBox();
						dialogBox.setText(caught.getMessage());
					}

					public void onSuccess(String risposta) {
						final MyDialogBox dialogBox = new MyDialogBox("=> "+risposta);
						listaRichieste.removeItem(indiceSelezionato);



					}

				});

			}

		});

		final Grid grid2 = new Grid(4, 4);

		grid2.setWidget(0, 0,  RichiestaRegistrazione);
		grid2.setWidget(0, 1,  listaRichieste);
		grid2.setWidget(2, 2,  buttonApprova2);
		grid2.setWidget(2, 1,  buttonRifiuta2);

		// agg. al panel
		vPanel.add(new HTML("<br>"));
		vPanel.add(titolo);		
		vPanel.add(grid);
		vPanel.add(new HTML("<br>"));
		vPanel.add(new HTML("<br>"));
		vPanel.add(titolo2);		
		vPanel.add(grid2);
		vPanel.add(new HTML("<br>"));


		vPanel.getElement().setAttribute("align", "center");
		RootPanel.get().add(vPanel);


	}

	/**
	 * Metodo per ottenere le richieste di Registrazione
	 */
	public void getRichiesteRegistrazione() {
		final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

		greetingService.getRichiesteRegistrazione(new AsyncCallback< ArrayList<String> >() {

			public void onFailure(Throwable caught) {

				MyDialogBox myDialogBox = new MyDialogBox(caught.getMessage());

			}

			@Override
			public void onSuccess( ArrayList<String> response) {
				for(int i=0; i< response.size(); i++) {
					listaRichieste.addItem(response.get(i));
				}
			}

		});
	}



}
