package progettosweng;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import progettosweng.client.Comando;
import progettosweng.client.Menu;

public class testClient {
 

	@Test
	public void testMenuTipo() {

		Menu m1 = new Menu(null, 1);
		Menu m2 = new Menu(null, 2);
		Menu m3 = new Menu(null, 3);
		Menu m4 = new Menu(null, 4);

		assertEquals(m1.getTipo(),1);
		assertEquals(m2.getTipo(),2);
		assertEquals(m3.getTipo(),3);
		assertEquals(m4.getTipo(),4);
	}

	@Test
	public void testComando() {

		// costruttore
		Comando comando = new Comando(null);
		assertNotNull("Comando è null", comando);

		//  Comando sconosicuto
		assertNull("Metodo ritorna not null", comando.comando("comandoSconosciuto"));
		// comandi riconosciuti
		assertNotNull("Metodo ritorna null", comando.comando("login"));
		assertNotNull("Metodo ritorna null", comando.comando("registrazione"));
		assertNotNull("Metodo ritorna null", comando.comando("Visualizza report"));
		assertNotNull("Metodo ritorna null", comando.comando("Esprimi voto"));
		assertNotNull("Metodo ritorna null", comando.comando("Richiesta giornalista"));
		assertNotNull("Metodo ritorna null", comando.comando("Presenta lista"));
		assertNotNull("Metodo ritorna null", comando.comando("Elezioni Concluse"));
		assertNotNull("Metodo ritorna null", comando.comando("Nomina funzionario"));
		assertNotNull("Metodo ritorna null", comando.comando("Bandisci elezione"));
		assertNotNull("Metodo ritorna null", comando.comando("Richieste"));
		assertNotNull("Metodo ritorna null", comando.comando("Profilo"));
		assertNotNull("Metodo ritorna null", comando.comando("Presenta lista"));
		assertNotNull("Metodo ritorna null", comando.comando("Presenta lista"));
		assertNotNull("Metodo ritorna null", comando.comando("Presenta lista"));
		assertNotNull("Metodo ritorna null", comando.comando("Presenta lista"));

	}



}
