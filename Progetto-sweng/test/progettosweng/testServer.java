package progettosweng;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import progettosweng.client.GreetingService;
import progettosweng.server.GreetingServiceImpl;
import progettosweng.shared.Documento;
import progettosweng.shared.Elezione;
import progettosweng.shared.FunzionarioComunale;
import progettosweng.shared.ListaElettorale;
import progettosweng.shared.UtenteRegistrato;
import progettosweng.shared.Voto;


public class testServer {

	GreetingServiceImpl servizio;


	/**
	 * Setup necessari per i test
	 */
	@Before
	public void setup() {
		
		GreetingService interfaccia = mock(GreetingService.class);
		servizio = new GreetingServiceImpl();
		// utilizzo Mockito
		servizio.setMockito(interfaccia);

	}

	/**
	 * Test inizializzazione
	 */
	@Test
	public void testInit() {
		
		// richiamo init()
		servizio.init();

		assertNotNull("Utenti map null", servizio.getUtentiMap());
		assertNotNull("Elezioni map null", servizio.getElezioniMap());
		assertNotNull("Lista Elettorale map null", servizio.getListaElettoraleMap());
		assertNotNull("Richieste giornalista map null", servizio.getRichiesteGiornalistaMap());
		assertNotNull("Voti map null", servizio.getVotiMappa());
		assertNotNull("Richieste utenti map null", servizio.getRichiesteUtentiMap());

	}

	/**
	 * Test per CalcolaAge
	 */

	@Test
	public void testCalcolaAge() {


		// richiamo init()
		servizio.init();

		assertEquals(21, servizio.calcolaAge("1996-12-12"));
		assertEquals(18, servizio.calcolaAge("2000-10-10"));
		assertEquals(27, servizio.calcolaAge("1991-01-17"));
		assertEquals(24, servizio.calcolaAge("1994-08-30"));
		assertEquals(15, servizio.calcolaAge("2002-12-09"));
		assertEquals(16, servizio.calcolaAge("2002-08-09"));
		assertEquals(15, servizio.calcolaAge("2003-08-09"));
		assertEquals(0, servizio.calcolaAge("2018-10-12"));
		assertEquals(28, servizio.calcolaAge("1990-10-12"));
	}

	/**
	 * Test per il Login
	 */
	@Test
	public void testLogin() {

		// richiamo init()
		servizio.init();
		assertEquals("1", servizio.login("funz", "funz") );
		assertEquals("2", servizio.login("admin", "admin") );
		assertEquals("3", servizio.login("ur1", "ur1") );
		assertEquals("0", servizio.login("utente in attesa", "passwordRandom") );

	}

	/**
	 * Test per GetUtentiRegistrati
	 */
	@Test
	public void testGetUtentiRegistrati() {

		// richiamo init()
		servizio.init();

		assertNotNull("Lista di utenti registrai Null", servizio.getUtentiRegistrati());
	}

	/**
	 * Test per Registrazione
	 */
	@Test
	public void testRegistrazione() {

		// richiamo init()
		servizio.init();

		// utente
		ArrayList<String> lista = new ArrayList<String>();
		lista.add("utent123");
		lista.add("utent123");
		lista.add("mario");
		lista.add("rossi");
		lista.add("3343398747");
		lista.add("mario.rossi@gmail.com");
		lista.add("MROS4567HB123");
		lista.add("Via Bellavista 1");
		lista.add("2018-10-10");
		lista.add("Passaporto");
		lista.add("P578-IJH1");
		lista.add("Comune");
		lista.add("2015-09-10");
		lista.add("2030-12-12");

		assertEquals("L'utente deve essere maggiorenne", servizio.registrazione(lista));

		// non minorenne : ok
		lista.set(8, "1996-11-11");
		assertNotEquals("L'utente deve essere maggiorenne", servizio.registrazione(lista));

		// username esiste nel DB
		lista.set(0, "ur1");
		lista.set(1, "ur1");
		assertEquals("Errore", servizio.registrazione(lista));

		// username non esiste nel DB
		lista.set(0, "usernameImpossibileDaGenerare");
		assertEquals("OK", servizio.registrazione(lista));


	}

	/**
	 * Test per BandisciElezione
	 */
	@Test
	public void testBandisciElezione() {

		// richiamo init()
		servizio.init();

		// creazline delle elezioni
		ArrayList<String> lista1 = new ArrayList<String>();
		lista1.add("elezione123");
		lista1.add("2018-01-01 18:20:55");
		lista1.add("2018-12-12 18:20:55");

		ArrayList<String> lista2 = new ArrayList<String>();
		lista2.add("elezione123");
		lista2.add("2018-01-01 18:20:55");
		lista2.add("2018-12-12 18:20:55");

		ArrayList<String> lista3 = new ArrayList<String>();
		lista3.add("elezione123-v2");
		lista3.add("2030-01-01 18:20:55");
		lista3.add("2018-12-12 18:20:55");

		assertEquals("OK,Elezione inserita correttamente", servizio.bandisciElezione(lista2, "funz"));

		// data non valida: inizio > fine
		assertEquals("Data non valida!!!", servizio.bandisciElezione(lista3, "funz"));

		//data non valida: inizio == fine
		lista3.set(1, "2018-12-12 18:20:55");
		assertEquals("Data non valida!!!", servizio.bandisciElezione(lista3, "funz"));

		servizio.getElezioniMap().remove("elezione123");
		servizio.getDb2().commit();

	}

	/**
	 * Test per PresentaLista
	 */
	@Test
	public void testPresentaLista() {

		// richiamo init()
		servizio.init();

		// lista elettorale: chiave doppia
		ListaElettorale lista = servizio.getListaElettoraleMap().get("listaElettoraleTest-elezione30");

		ArrayList <String> listaString = new ArrayList<String>();

		String candidatiBuffer = "";

		// buffer per i candidati
		for (UtenteRegistrato x : lista.getCandidati()) {
			candidatiBuffer+= "\t"+x.getUsername();

		}

		// creazione elezione
		Elezione ele = servizio.getElezioniMap().get("elezione30");
		String ElezioneString = ele.getnome()+"\t";
		listaString.add(ElezioneString);
		listaString.add(lista.getNome());
		listaString.add(lista.getSimbolo());
		listaString.add(lista.getSindaco().getUsername());
		listaString.add(candidatiBuffer);

		assertNotNull("Errore : lista gia' inserita", servizio.presentaLista(listaString, "ur10"));

	}

	/**
	 * Test per RichiestaAccreditamentoGiornalista
	 */
	@Test
	public void testRichiestaAccreditamentoGiornalista() {

		// richiamo init()
		servizio.init();

		Documento doc = new Documento("Passaporto","AS1234567890","Comune","2010-02-02","2030-02-02");
		UtenteRegistrato urx = new UtenteRegistrato("Laura","Rossi","3343329292","laura.roro@gmail.com","LA45FGHYLELL001",
				"Via Marconi 23/A","1990-11-30","ur-101","ur-101",doc);

		servizio.getUtentiMap().put("ur-101", urx);
		// accreditamento
		assertNotEquals("Errore richiesta gia' inviata", servizio.RichiestaAccreditamentoGiornalista("ur-101"));

		servizio.getRichiesteGiornalistaMap().remove("ur-101");
		servizio.getDb2().commit();

	}

	/**
	 * Test per GetListaRichiesteGiornalista
	 */
	@Test
	public void testGetListaRichiesteGiornalista() {

		// richiamo init()
		servizio.init();
		assertNotNull("Errore: Lista Richieste Giornalista Null", servizio.getListaRichiesteGiornalista());

	}

	/**
	 * Test per SetApprovaRichiestaGiornalista
	 */
	@Test
	public void testSetApprovaRichiestaGiornalista() {

		// richiamo init()
		servizio.init();

		Documento doc = new Documento("Passaporto","AS1234567890","Comune","2010-02-02","2030-02-02");
		// creo l'utente registrato
		UtenteRegistrato utenteTest3 = new UtenteRegistrato("Fabio","Rosso",
				"3343329899","FabRoss@gmail.com","ROS6789JHBFAB1",
				"Via del pratello 7/c","1950-12-11","utenteTest3","utenteTest3",doc);

		servizio.getUtentiMap().put("utenteTest3", utenteTest3);
		servizio.RichiestaAccreditamentoGiornalista("utenteTest3");
		assertEquals("L'utente : utenteTest3  e' stato accreditato come giornalista", servizio.setApprovaRichiestaGiornalista("utenteTest3"));
		servizio.getUtentiMap().remove("utenteTest3");
		servizio.getDb2().commit();
	}

	/**
	 * Test per SetRifiutaRichiestaGiornalista
	 */
	@Test
	public void testSetRifiutaRichiestaGiornalista() {

		// richiamo init()
		servizio.init();

		ArrayList<String> lista = new ArrayList<String>();
		lista.add("userTest4");
		lista.add("userTest4");
		lista.add("Enrico");
		lista.add("Rossi");
		lista.add("3343398747");
		lista.add("enry_Red@gmail.com");
		lista.add("ENOS4567HB123");
		lista.add("Via Zannoni 88");
		lista.add("2018-10-10");
		lista.add("Passaporto");
		lista.add("P578-IJH1");
		lista.add("Comune");
		lista.add("2015-09-10");
		lista.add("2030-12-12");

		servizio.registrazione(lista);
		servizio.ApprovaRichiestaRegistrazione("userTest4");
		servizio.RichiestaAccreditamentoGiornalista("userTest4");

		assertEquals("Richiesta rigettata per utente userTest4", servizio.setRifiutaRichiestaGiornalista("userTest4"));
	}

	/**
	 * Test per GetInfoUsername
	 */
	@Test
	public void testGetInfoUsername() {

		// richiamo init()
		servizio.init();
		// non trovato
		assertEquals("", servizio.getInfoUsername("Username-Non_esiste"));
		// trovato
		assertNotEquals("", servizio.getInfoUsername("ur1"));

	}

	/**
	 * Test per GetListaElettorale
	 */
	@Test
	public void testGetListaElettorale(){

		// richiamo init()
		servizio.init();

		assertNotNull("lista Null", servizio.getListaElettorale());
	}

	/**
	 * Test per GetListaElettoraleApprovata
	 */
	public void testGetListaElettoraleApprovata(){

		// null
		assertNotNull("lista Null", servizio.getListaElettoraleApprovata());
		// 0
		assertNotEquals(0, servizio.getListaElettoraleApprovata().size());
	}

	/**
	 * Test per GetElezioneInCorso
	 */
	public void testGetElezioneInCorso(){

		// null
		assertNotNull("il valore è null", servizio.getElezioneInCorso());
		// 0
		assertNotEquals(0, servizio.getElezioneInCorso().size());
	}


	/**
	 * Test per GetElezioniConcluse
	 */
	@Test
	public void testGetElezioniConcluse() {

		// richiamo init()
		servizio.init();

		//not null
		assertNotNull("lista è null", servizio.getElezioniConcluse("2030-01-01 00:00:00"));
		// non vuota
		assertNotEquals(0, servizio.getElezioniConcluse("2030-01-01 00:00:00") );

	}

	/**
	 * Test per GetInfoLista
	 */
	@Test
	public void testGetInfoLista() {

		// richiamo init()
		servizio.init();
		// lista non esiste
		assertEquals("", servizio.getInfoLista("lista-Non-esistente___"));

	}

	/**
	 * Test per EsprimiVoto
	 */
	@Test
	public void testEsprimiVoto() {

		// richiamo init()
		servizio.init();
		ArrayList<String> lista = new ArrayList<String>();
		lista.add("ur10");
		lista.add("elezione30");
		lista.add("listaElettoraleTest");
		lista.add("No preferenza");

		assertNotEquals("OK, voto inserito ", servizio.esprimiVoto(lista));

	}

	/**
	 * Test per GetProfiloUtente
	 */
	@Test
	public void testGetProfiloUtente() {

		// richiamo init()
		servizio.init();

		// utente non esiste
		assertEquals("", servizio.getProfiloUtente("utente.non!esiste"));
		// utetne trovato
		assertNotEquals("", servizio.getProfiloUtente("ur1"));

	}

	/**
	 * Test per GetRichiesteUtente
	 */
	@Test
	public void testGetRichiesteUtente() {

		// richiamo init()
		servizio.init();

		Documento doc = new Documento("Passaporto","AS1234567890","Comune","2010-02-02","2030-02-02");
		UtenteRegistrato utenteTest4 = new UtenteRegistrato("laura","lella","3343329292","email@gmail.com","CodiceFiscaleProva",
				"indirizzoProva","1990-11-11","utenteTest4","utenteTest4",doc);

		servizio.getUtentiMap().put("utenteTest4", utenteTest4);

		servizio.getRichiesteGiornalistaMap().put("utenteTest4", "utenteTest4");
		servizio.getDb2().commit();

		assertEquals(1, servizio.getRichiesteUtente("utenteTest4").size());

	}

	/**
	 * Test per NominaFunzionario
	 */
	@Test
	public void testNominaFunzionario() {

		// richiamo init()
		servizio.init();

		// utente
		ArrayList<String> lista = new ArrayList<String>();
		lista.add("funz2-2");
		lista.add("funz2-2");
		lista.add("Enrico");
		lista.add("Rossi");
		lista.add("3343398747");
		lista.add("enry_Red@gmail.com");
		lista.add("ENOS4567HB123");
		lista.add("Via Zannoni 88");
		lista.add("1990-10-10");
		lista.add("Passaporto");
		lista.add("P578-IJH1");
		lista.add("Comune");
		lista.add("2013-09-10");
		lista.add("2020-09-10");


		servizio.registrazione(lista);
		servizio.ApprovaRichiestaRegistrazione("funz2-2");

		assertNotEquals("", servizio.nominaFunzionario("funz2-2"));

		assertEquals("Operazione eseguita con successo!", servizio.nominaFunzionario("funz2-2"));

	}

	/**
	 * Test per GetUtentiRegistrati2
	 */
	@Test
	public void testGetUtentiRegistrati2() {

		// richiamo init()
		servizio.init();

		// null
		assertNotNull("lista Null", servizio.getUtentiRegistrati2());
		// 0
		assertNotEquals(0, servizio.getUtentiRegistrati2().size());


	}

	/**
	 * Test per GetRichiesteRegistrazione
	 */
	@Test
	public void testGetRichiesteRegistrazione() {

		// richiamo init()
		servizio.init();

		ArrayList<String> lista = new ArrayList<String>();
		lista.add("utenteTest5");
		lista.add("utenteTest5");
		lista.add("Enrico");
		lista.add("Rossi");
		lista.add("3343398747");
		lista.add("enry_Red@gmail.com");
		lista.add("ENOS4567HB123");
		lista.add("Via Zannoni 88");
		lista.add("1990-10-10");
		lista.add("Passaporto");
		lista.add("P578-IJH1");
		lista.add("Comune");
		lista.add("2013-09-10");
		lista.add("2020-09-10");

		servizio.registrazione(lista);

		// null
		assertNotNull("lista Null", servizio.getRichiesteRegistrazione());
		// 0
		assertNotEquals(0, servizio.getRichiesteRegistrazione().size());


	}

	/**
	 * Test per RifiutaRichiestaRegistrazione
	 */
	@Test
	public void testRifiutaRichiestaRegistrazione() {

		// richiamo init()
		servizio.init();

		// utente
		ArrayList<String> lista = new ArrayList<String>();
		lista.add("utenteTest5");
		lista.add("utenteTest5");
		lista.add("Enrico");
		lista.add("Rossi");
		lista.add("3343398747");
		lista.add("enry_Red@gmail.com");
		lista.add("ENOS4567HB123");
		lista.add("Via Zannoni 88");
		lista.add("1990-10-10");
		lista.add("Passaporto");
		lista.add("P578-IJH1");
		lista.add("Comune");
		lista.add("2013-09-10");
		lista.add("2020-09-10");

		servizio.registrazione(lista);

		assertEquals("Operazione eseguita con successo!", servizio.RifiutaRichiestaRegistrazione("utenteTest5"));

	}

	/**
	 * Test per ApprovaRichiestaRegistrazione
	 */
	@Test
	public void testApprovaRichiestaRegistrazione() {

		// richiamo init()
		servizio.init();

		Documento doc = new Documento("Passaporto","AS1234567890","Comune","2010-02-02","2030-02-02");

		UtenteRegistrato utenteTest = new UtenteRegistrato("Carlo","Rossi",
				"3343329292","emailDiProva@gmail.com","DFT123KJHBV",
				"via della Prova 1","1911-11-11","ur10","ur10",doc);

		UtenteRegistrato utenteTest2 = new UtenteRegistrato("Fabiana","Rossi",
				"3343329292","fabRossy@gmail.com","ROSGHJ239BN2",
				"via degli Rossi","1910-10-11","utenteTest2","utenteTest2",doc);

		FunzionarioComunale funzionarioTest = new FunzionarioComunale("Paolo","Rossi","051294938","p.rossi@gmail.com","PAROSS53HF378","Via degli Orti 12","10/08/1953","funz","funz",doc);


		Elezione ele = new Elezione("elezione30", "2018-01-01", "2018-09-01", "00:00:00", "00:00:00", "", funzionarioTest);
		ArrayList <UtenteRegistrato> candidatiTest = new ArrayList<UtenteRegistrato>();
		candidatiTest.add(utenteTest);



		ListaElettorale listaElettoraleTest = new ListaElettorale("TestNomeLista1", "TestsimboloLista", "Pendente",utenteTest2,candidatiTest, ele, funzionarioTest, utenteTest);
		Voto v = new Voto(utenteTest,listaElettoraleTest,"No preferenza",ele);

		servizio.getRichiesteUtentiMap().put("utenteTest2", utenteTest2);


		servizio.getVotiMappa().put("ur10-elezione30",v);
		servizio.getUtentiMap().put("ur10", utenteTest);

		servizio.getElezioniMap().put("elezione30", ele);
		servizio.getListaElettoraleMap().put("listaElettoraleTest-elezione30", listaElettoraleTest);

		// utente
		ArrayList<String> lista = new ArrayList<String>();
		lista.add("utent123z_vc");
		lista.add("utent123z-vc");
		lista.add("Enrica");
		lista.add("di Rossi");
		lista.add("3343398747");
		lista.add("diRossi.en@gmail.com");
		lista.add("ENOS4567HB123");
		lista.add("Via Zannoni 100");
		lista.add("1990-10-10");
		lista.add("Passaporto");
		lista.add("P578-IJH1");
		lista.add("Comune");
		lista.add("2013-09-10");
		lista.add("2020-09-10");
		

		servizio.registrazione(lista);

		assertNotEquals("", servizio.ApprovaRichiestaRegistrazione("utent123z-vc"));
		assertEquals("Operazione eseguita con successo!", servizio.ApprovaRichiestaRegistrazione("utenteTest2"));

	}

	/**
	 * Test per GetReport
	 */
	@Test
	public void testGetReport() {

		// richiamo init()
		servizio.init();

		// not null
		assertNotNull("lista Null", servizio.getReport("id-elezione"));
		// no uguale a 0
		assertNotEquals(0, servizio.getReport("id-elezione").size());


		servizio.getElezioniMap().clear();
		servizio.getUtentiMap().clear();
		servizio.getVotiMappa().clear();
		servizio.getRichiesteGiornalistaMap().clear();
		servizio.getRichiesteUtentiMap().clear();

		servizio.init();

	}



}
